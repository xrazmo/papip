#!/bin/bash -l

####################################
#     ARIS slurm script template   #
#                                  #
# Submit script: sbatch filename   #
#                                  #
####################################

#SBATCH -A snic2021-5-59    # Replace with your system project
#SBATCH -p core  # Partition
#SBATCH -n 1     # Number of tasks(ntask)
#SBATCH -c 16     # number of cpus required per task
#SBATCH -J clustering_mmseq2   # Job name



#SBATCH --output=sl_%x_%j.out # Stdout (%j expands to jobId)
#SBATCH --error=sl_%x_%j.err # Stderr (%j expands to jobId)


#SBATCH --mem=16G   # memory per NODE

#IS THIS A TEST RUN?

#SBATCH --time=00:15:00   # walltime
#SBATCH --qos=short

if [ x$SLURM_CPUS_PER_TASK == x ]; then
  export OMP_NUM_THREADS=1
else
  export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
fi


## LOAD MODULES ##
module purge		# clean up loaded modules

# load necessary modules
module load python/3.7.2
module load bioinfo-tools
module load biopython/1.76-py3
#module load blast/2.9.0+
#module load prodigal/2.6.3
#module load MUMmer/4.0.0rc1
#module load diamond/2.0.4

## RUN YOUR PROGRAM ##
srun python -u /domus/h1/xrazmo/private/source_codes/papip/workDesk.py
