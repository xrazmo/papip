import os
from glob import glob
import json
from BioLib.Utility import Runner, DataBaseManager


class Diamond:

    @staticmethod
    def run(inDir, outDir, diamondDB_json=None, samples_file=None, threads=1, task='x'
                            , outCols="-f 6 qseqid sseqid pident mismatch gaps evalue bitscore length "
                                      "qlen slen qstart qend sstart send stitle"):
        print(f"\n----Running Diamond----")

        try:
            tmpDir = os.path.join(outDir, f"tmp")
            os.mkdir(tmpDir)
            print(f"Make-Dir:{tmpDir}")
        except Exception as e:
            print(f"Error:{e}")
            tmpDir = outDir

        os.chdir(tmpDir)

        if diamondDB_json is None:
            raise ValueError("No reference database is provided!")

        with open(diamondDB_json) as hdl:
            diamondDBs = json.load(hdl)

        samples = []
        if samples_file is not None:
            with open(samples_file) as hdl:
                for ll in hdl:
                    smp = ll.strip('\r\n')
                    if len(smp) > 1:
                        samples.append(smp)
        else:
            samples = ["*"]

        qryFiles = []
        for smp in samples:
            qryFiles += glob(os.path.join(inDir, f"{smp}*.fna"))

        print(f'\n\t{len(qryFiles)} input files were detected.\n')

        for dbDic in diamondDBs:
            cmdList = []
            for ff in qryFiles:
                filename = os.path.basename(ff).split('.')[0]
                outb6 = os.path.join(tmpDir, f"{dbDic['name']}__{filename}.b6")
                if task.startswith('x'):
                    cmdList.append(
                        f'diamond blastx -p {threads} -o {outb6}.part {dbDic["option"]} -d {dbDic["path"]} -q {ff} {outCols}; mv {outb6}.part {outb6}')
                elif task.startswith('p'):
                    cmdList.append(
                        f'diamond blastp -p {threads} -o {outb6}.part {dbDic["option"]} -d {dbDic["path"]} -q {ff} {outCols}; mv {outb6}.part {outb6}')
                else:
                    raise ValueError(f"Unrecognized task type (x:blastx,p:blastp): task={task}")
            Runner.os_parallel(cmdList, 2, quiet=False)

    @staticmethod
    def saveResultsDirectory(b6Dir, patterns,task='x'):
        print(f"\n----Saving results into the reference database----")
        db=Diamond.__createDB(b6Dir)
        conn,cur = DataBaseManager.connect(db)

        logfile = os.path.join(os.path.dirname(db), "logERROR_insertDiamond.txt")
        print(f"\n\tCheck possible errors in: {logfile}\n")

        hdl_log = open(logfile, 'w+')
        collist =["qseqid", "sseqid", "pident", "mismatch", "gaps", "evalue", "bitscore",
                  "length", "qlen", "slen", "qstart", "qend", "sstart", "send", "stitle"]
        colsDic = {col.lower(): i for i, col in enumerate(collist)}

        b6Files = []
        for pt in patterns:
            b6Files += sorted(glob(os.path.join(b6Dir, pt)))

        for ff in b6Files:
            i = 0
            dbname = os.path.basename(ff).split('__')[0]
            errorCount = 0
            with open(ff) as hdl:
                print(f"\t{ff}")
                for ll in hdl:
                    i += 1
                    cols = ll.strip('\n\r').split('\t')
                    if len(cols) < 12:
                        continue
                    GiveVal = lambda name: cols[colsDic[name]]
                    alignmentLength = float(GiveVal("length"))
                    qlen = float(GiveVal("qlen")) / (1., 3.)[task.startswith('x')]
                    qcov = round(100.0 * alignmentLength / qlen)
                    scov = round(100.0 * alignmentLength / float(GiveVal("slen")))
                    stitle = GiveVal("stitle").replace(GiveVal("sseqid"), '').strip(' ')
                    sseqid = GiveVal("sseqid")
                    if dbname=='vfdb':
                        stitle = stitle.split()[0].strip('()')
                        sseqid = sseqid.split('|')[-1].strip(')')
                    elif dbname=='victor':
                        stitle = stitle.split('[')[0].strip('()')
                        sseqid = sseqid.split('|')[-2].strip(')')

                    try:
                        cur.execute(
                            "INSERT INTO tbl_blastxDiamond (orfAcc,refId,identity,qcov,scov,length,qlen,slen,mismatch,gap,"
                            "qstart,qend,sstart,send,eValue,bitScore,dbName,description) VALUES"
                            " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                            (GiveVal("qseqid"), sseqid, GiveVal("pident"), qcov, scov, GiveVal("length")
                             , GiveVal("qlen"), GiveVal("slen"), GiveVal("mismatch"), GiveVal("gaps"),
                             GiveVal("qstart"), GiveVal("qend"), GiveVal("sstart"), GiveVal("send"),
                             GiveVal("evalue"), GiveVal("bitscore"), dbname, stitle))
                    except Exception as e:
                        errorCount += 1
                        hdl_log.write(f"{errorCount}- Error in line number {i} of {os.path.basename(ff)} "
                                      f"\n\toriginal line:{ll}"
                                      f"\n\tException:'{e}'")
            if errorCount > 0:
                print(
                    f"\t detected while processing {ff}")

            conn.commit()
            # os.rename(ff, ff + '-')
        hdl_log.close()
        conn.close()

    @staticmethod
    def findBestHits(dbPath, marker="*"):
        print(f"\n----Marking the best hits----")
        conn,cur = DataBaseManager.connect(dbPath)

        print('\tRemoving tags...')
        conn.execute("update tbl_blastxDiamond set tag=null;")
        conn.commit()
        mode = 1
        print(f'\tFinding different reference database')
        refDbs = [rr[0] for rr in cur.execute("select distinct dbname from tbl_blastxDiamond;")]
        print(f"\t\tDetected {len(refDbs)} reference database:  {', '.join(refDbs)}")
        bestHitformual = ("max(qcov*identity)", "max(bitScore)")[ mode== 2]
        for dbname in refDbs:
            print(f'\n\t-Fetching hits in {dbname}', end='')
            cmd = f"select Id,orfAcc,{bestHitformual} from tbl_blastxDiamond where dbName='{dbname}' group by orfAcc;"
            ids = [0]
            rows = cur.execute(cmd).fetchall()
            cnt = 0
            for rr in rows:
                ids.append(rr[0])
                cur.execute("update tbl_blastxDiamond set tag=? where id=?", (marker, rr[0]))
                cnt += 1
            print(f"\t{cnt} rows will be updated.")
        conn.commit()
        conn.close()
        print(f"\tChanges were saved!")
        print(f'\nDone!')

    @staticmethod
    def __createDB(outDir):
        db = os.path.join(outDir,'tmp_blastxDiamond.db')
        try:
            conn, cur = DataBaseManager.connect(db)
            conn.execute("CREATE TABLE tbl_blastxDiamond (Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,orfAcc VARCHAR, "
                         "refId VARCHAR,identity FLOAT,qcov FLOAT, scov FLOAT, length INTEGER,qlen INTEGER, "
                         "slen INTEGER,mismatch INTEGER,gap INTEGER,qstart INTEGER,qend INTEGER,sstart INTEGER, send INTEGER,"
                         "eValue VARCHAR, bitScore FLOAT,dbName VARCHAR, description VARCHAR,tag VARCHAR);")
            conn.commit()
            conn.close()
        except Exception as e:
            print(e)

        return db

    @staticmethod
    def import2RefDB(refDB,tmpDB,condi=None):
        conn,cur=DataBaseManager.connect(refDB)
        print('Attach database...')
        cur.execute(f"attach database \"{tmpDB}\" as tmpDB")
        if condi is not None:
            condi = f" where {condi}"

        cmd = f"insert into blastxdiamond (proId, refId, identity, qcov, scov, length, qlen, slen, mismatch," \
              f" gap, qstart, qend, sstart, send, eValue, bitScore, dbName, description) " \
              f"select orfAcc, refId, identity, qcov, scov, length, qlen, slen, mismatch," \
              f" gap, qstart, qend, sstart, send, eValue, bitScore, dbName, description from tmpDB.tbl_blastxdiamond {condi}"


        cur.execute(cmd)
        conn.commit()
        conn.close()