import os
from glob import glob
from subprocess import Popen, PIPE
from time import time
from shlex import split
import sqlite3 as lit


class Extractor():
    @staticmethod
    def glob_input(inDir, outDir, threads=1, extension_re='*.fna.gz', quiet=False):
        print(f"\n{25 * '-'}\n{10 * '*'}EXTRACTING FILES BY USING 'GLOB' COMMAND IN INPUT DIRECTORY{10 * '*'}")
        print(f"##input-dir:{inDir}")
        print(f"##out-dir:{outDir}")
        print(f"##extension:{extension_re}\n{25 * '-'}")
        print(f"##Quiet:{quiet}\n{25 * '-'}")

        fnaList = glob(os.path.join(inDir, extension_re))
        cmdList = []
        print('\tCreating commands...')
        os.chdir(outDir)
        for ff in fnaList:
            fname = os.path.basename(ff).split('.gz')[0]
            cmdList.append(f'gzip -dc {ff} > {fname}.part; mv {fname}.part {fname}')

        Runner.os_parallel(cmdList, threads, quiet)
        print(f"\n{10 * '*'}glob_input-FINISHED{10 * '*'}\n{25 * '-'}")

    @staticmethod
    def find_input(inDir, outDir, threads=1, name_re='*.gz', quiet=False):
        print(f"\n{25 * '-'}\n{10 * '*'}EXTRACTING FILES BY USING 'FIND' COMMAND IN INPUT DIRECTORY{10 * '*'}")
        print(f"##input-dir:{inDir}")
        print(f"##out-dir:{outDir}")
        print(f"##name:{name_re}")
        print(f"##threads:{threads}")
        print(f"##Quiet:{quiet}\n{25 * '-'}")

        cmdList = []
        fnaList = Runner.read_stdout(f"find {inDir} -name {name_re}")
        print('\tCreating commands...')
        os.chdir(outDir)
        for ff in fnaList:
            fname = os.path.basename(ff).split('.gz')[0]
            cmdList.append(f'gzip -dc {ff} > {fname}.part; mv {fname}.part {fname}')

        Runner.os_parallel(cmdList, threads, quiet)
        print(f"\n{10 * '*'}glob_input-FINISHED{10 * '*'}\n{25 * '-'}")


class Comperssor():

    @staticmethod
    def list_tarGZ(inFile, inDir, outDir, threads=16, quiet=False):
        print(f"\n{25 * '-'}\n{10 * '*'}Compressing file: multiple server{10 * '*'}")
        print(f"##input-file:{inFile}")
        print(f"##in-dir:{inDir}")
        print(f"##out-dir:{outDir}")
        print(f"##threads:{threads}")
        print(f"##Quiet:{quiet}\n{25 * '-'}")
        cmdList = []
        os.chdir(inDir)
        with open(inFile) as hdl:
            for ll in hdl:
                sample = ll.strip('\r\n')
                outf = os.path.join(outDir, sample)
                if len(ll) > 1:
                    cmdList.append(
                        f'tar {sample}* | pbzip2 -vkcr -p{threads} {sample}* > {sample}.tar.bz2.part; mv {sample}.tar.bz2.part {outf}.tar.bz2')

        Runner.os_parallel(cmdList, 1, quiet)


class QualityController:
    @staticmethod
    def fastqc(inDir, outDir, extension_re="*.fastq", threads=1, quiet=False):
        fqList = glob(os.path.join(inDir, extension_re))
        cmdList = []
        for ff in fqList:
            cmdList.append('fastqc -q -o {0} {1}'.format(outDir, ff))
        Runner.os_parallel(cmdList, threadNumber=threads, quiet=quiet)
        print(f"\n{10 * '*'}fastqc-FINISHED{10 * '*'}\n{25 * '-'}")

    @staticmethod
    def trimGalor(inDir, outDir, extension_re='*.gz', threads=1, quiet=False):
        print(f"\n{25 * '-'}\n{10 * '*'}TRIM_GALORE: FILTERING AND TRIMMING READS{10 * '*'}")
        print(f"##input-dir:{inDir}")
        print(f"##out-dir:{outDir}")
        print(f"##threads:{threads}")
        print(f"##quiet:{quiet}\n{25 * '-'}")

        fqList = sorted(glob(os.path.join(inDir, extension_re)))
        cmdList = []
        for i in range(0, len(fqList), 2):
            r1 = fqList[i]
            r2 = fqList[i + 1]
            cmdList.append(
                f'trim_galore --retain_unpaired --paired -q 20 --length 20 --dont_gzip -o {outDir} {r1} {r2}')
        Runner.os_parallel(cmdList, threadNumber=threads, quiet=quiet)
        print(f"\n{10 * '*'}trimGalor-FINISHED{10 * '*'}\n{25 * '-'}")


class Runner():
    @staticmethod
    def os_parallel(commandLst, threadNumber, quiet=True):
        print(f"\n{25 * '-'}\n{10 * '*'}START RUNNING LIST OF COMMANDS{10 * '*'}")
        print(f"##Nr. commands:{len(commandLst)}")
        print(f"##Thread:{threadNumber}")
        print(f"##Quiet:{quiet}\n{25 * '-'}")

        processes = set()
        for i, cmd in enumerate(commandLst):
            if not quiet:
                print(f"\t{i}/{len(commandLst)}:{cmd}")
            processes.add(Popen(cmd, shell=True))
            if len(processes) >= threadNumber:
                os.wait()
                processes.difference_update([p for p in processes if p.poll() is not None])

        for p in processes:
            if p.poll() is None:
                p.wait()

        print(f"\n{10 * '*'}os_parallel-FINISHED{10 * '*'}\n{25 * '-'}")

    @staticmethod
    def read_stdout(command):
        process = Popen(split(command), stdout=PIPE)
        outlst = []
        while True:
            output = str(process.stdout.readline()).strip("\\n'b")
            if len(output) < 2 and process.poll() is not None:
                break
            if output:
                outlst.append(output.strip())
        return outlst


class DataBaseManager():
    @staticmethod
    def connect(dbPath):
        conn = lit.connect(dbPath)
        cur = conn.cursor()
        return conn, cur

    @staticmethod
    def vaccum(dbPath):
        raise NotImplementedError

    @staticmethod
    def runCommands_dir(sqlDir, db,has_commit=False,stdout=True):
        for ff in glob(os.path.join(sqlDir,'*.sql')):
            DataBaseManager.runCommands(ff,db,has_commit=has_commit,stdout=stdout)

    @staticmethod
    def runCommands(sqlFile, db,has_commit=False,stdout=True):
        print(sqlFile)
        conn,cur = DataBaseManager.connect(db)
        if not stdout:
            hdl_log = open(os.path.join(os.path.dirname(sqlFile), "log.txt"), 'w+')
        tic = time()
        prog=0
        with open(sqlFile) as hdl:
            for ll in hdl:
                prog+=1
                if time()-tic>1:
                    print(f"\rProgress {prog}.",end='')
                    tic=time()
                ll = ll.rstrip('\n')
                if len(ll) > 1:
                    try:
                        cur.execute(ll)
                    except Exception as e:
                        msg = f"{10 * '!'}ERROR: {e}.## {ll} ##"
                        if not stdout:
                            hdl_log.write(msg)
                        else:
                            print(msg)
        if has_commit:
            conn.commit()
        os.rename(sqlFile, sqlFile + '-')
        conn.close()
        if not stdout:
            hdl_log.close()


    @staticmethod
    def nextNonZeroID(id):
        id += 1
        while str(id).find('0') > -1:
            id += 1
            continue
        return id


class FastaManager:

    @staticmethod
    def concatFiles(indir, outDir, name, extension):
        outF = os.path.join(outDir, name)
        cmd = f"for f in {indir}/*.{extension}; do (cat \"${{f}}\"; echo) >> {outF} ; done"
        Popen(cmd, shell=True).wait()
        return outF

    @staticmethod
    def splitFasta(inFasta, outDir, splitSize=10):
        print(f'Spliting {inFasta} to {splitSize}')
        infileName = os.path.basename(inFasta).split('.')[0]

        print('Creating buffers...')

        buffers = [[] for _ in range(splitSize)]
        buffSize = 5e4

        tic = time()

        print('Creating chunk files...')
        handels = []
        for i in range(splitSize):
            outF = os.path.join(outDir, f"{infileName}_chunk{i + 1}.fna")
            handels.append(open(outF, 'w+'))

        print('Going through the lines...')

        bufferIter = 0
        seqRec = []
        with open(inFasta) as hdl:
            lineCounter = -1
            for ll in hdl:
                lineCounter += 1
                if ll.startswith('>'):
                    if lineCounter > 0:
                        # save the records in the buffers
                        buffers[bufferIter % splitSize] += seqRec
                        bufferIter += 1
                        seqRec = []

                seqRec.append(ll)
                toc = time()
                if toc - tic > 2:
                    print(f'\r{lineCounter}', end='', flush=True)
                    tic = time()

                if bufferIter % buffSize == 1:
                    for i in range(splitSize):
                        handels[i].write(''.join(buffers[i]))
                    print(f'\r...saved!', end='', flush=True)
                    bufferIter += 1
                    # clear the buffers
                    buffers = [[] for _ in range(splitSize)]

        for i in range(splitSize):
            handels[i].write(''.join(buffers[i]))

        print('Closing Files...')

        for hdl in handels:
            hdl.close()

        print('Done!')
