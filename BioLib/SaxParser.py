import os
from xml.sax.handler import ContentHandler
from xml.sax import make_parser, handler
import pandas as pd

class BioSampler(ContentHandler):

    def __init__(self):
        self.cur_tag = None
        self.cur_attr = None
        self.dataDic = {}
        self.dataCollect = []
        self.flag = False

    def startElement(self, tag, attrs):
        if tag == 'Attribute':
            self.cur_attr = attrs.get('attribute_name')
        elif tag == 'BioSample':
            self.dataDic["accession"]=attrs.get("accession")
            self.dataDic["publication_date"]=attrs.get("publication_date")
            self.dataDic["submission_date"]=attrs.get("submission_date")
        elif tag == 'Organism':
            self.dataDic["taxonomy_name"]=attrs.get("taxonomy_name")
            self.dataDic["taxonomy_id"]=attrs.get("taxonomy_id")

        self.cur_tag = tag

    def endElement(self, tag):
        if tag=="BioSample":
            self.dataCollect.append(self.dataDic)
            self.dataDic = {}

    def characters(self, content):
        if len(content.strip('\n ')) > 0:
            if self.cur_tag == 'Attribute':
                self.dataDic[self.cur_attr] = content
            if self.cur_tag == 'Title':
                self.dataDic["Title"] = content

    def parse(self, infile,outDir):
        with open(infile) as hdl:
            parser = make_parser()
            parser.setFeature(handler.feature_namespaces, 0)
            parser.setContentHandler(self)
            parser.parse(hdl)

        df = pd.DataFrame(self.dataCollect)
        df.to_csv(os.path.join(outDir,'BioSample_parseOut.csv'))
        # return self.dataCollect

