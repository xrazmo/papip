import datetime
import os
import sqlite3 as lit
from glob import glob
from multiprocessing.pool import Pool
from .Utility import Runner
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord

__author__ = 'xrazmo'


class ProdigalResult:
    def __init__(self):
        self.tag = ''
        self.no = ''
        self.startIdx = 0
        self.endIdx = 0
        self.strand = ''
        self.partial = ''
        self.startType = ''
        self.rbsMotif = ''
        self.rbsSpacer = ''
        self.gccontent = 0
        self.conf = 0
        self.csscore = 0
        self.ssscore = 0
        self.rscore = 0
        self.uscore = 0
        self.tscore = 0
        self.proseq = ''
        self.dnaseq = ''


class Prodigal:
    @staticmethod
    def run(inDir,outDir,samples_file=None,extension="fna",out_nucl=False,out_prot=False,threads=1):
        print(f"\n----Running Prodigal----")

        try:
            outDir = os.path.join(outDir, f"tmp")
            os.mkdir(outDir)
            print(f"Make-Dir:{outDir}")
        except Exception as e:
            print("{e}")

        os.chdir(outDir)

        fileNames = []
        samples = []
        if samples_file is not None:
            with open(samples_file) as hdl:
                for ll in hdl:
                    smp = ll.strip('\r\n')
                    if len(smp)>1:
                        samples.append(smp)
        else:
            samples=["*"]

        for smp in samples:
            fileNames += glob(os.path.join(inDir, f"{smp}.{extension}"))

        print(f'\n\t{len(fileNames)} input files were detected.\n')

        cmdList = []
        for ff in fileNames:
            fname = os.path.basename(ff).split('.')[0]

            outCoordF = os.path.join(outDir, f'{fname}_coords.txt')

            tcommand = f"prodigal -i {ff} -o {outCoordF} "

            if out_prot:
                proteinF = os.path.join(outDir, f'{fname}_proseq.faa')
                tcommand = tcommand + f" -a {proteinF}"
            if out_nucl:
                dnaF = os.path.join(outDir, f'{fname}_dnaSeq.fna')
                tcommand = tcommand + f" -d {dnaF}"

            tcommand = tcommand + " -f gff -q -m"
            cmdList.append(tcommand)
        Runner.os_parallel(cmdList,threads,quiet=False)

    @staticmethod
    def saveResult(dbPath, coordDir, saveGene, saveProtein):
        print(f'\n-----Saving ORFs-----')
        print(f"\t{os.path.abspath(dbPath)}\n")

        coordFiles = glob(os.path.join(coordDir, '*coords*'))
        conn = lit.connect(dbPath)
        cur = conn.cursor()

        cmd = "select id, lower(substr(fq_1,1,instr(fq_1,'_1.fastq')-1)) from isolates " \
              " where ticket not in ('failed','doublettes')"
        sampleDic = {rr[1]: rr[0] for rr in cur.execute(cmd)}

        print(f'\tReading coord files:')
        for i,cfn in enumerate(coordFiles):
            print(f"\t\t{i+1}/{len(coordFiles)}:{cfn}\n")
            fid = os.path.basename(cfn).split('_coords')[0]
            geneFasta = (None, os.path.join(coordDir, '{}_dnaSeq.fna'.format(fid)))[saveGene]
            ORFFasta = (None, os.path.join(coordDir, '{}_proSeq.faa'.format(fid)))[saveProtein]

            if (not Prodigal.parseCoordFiles(cfn, cur, geneFasta, ORFFasta,sampleDic)):
                return False
        conn.commit()
        conn.close()
        print(f"\tORFs were saved in the reference database successfully!")
        return True

    @staticmethod
    def parseCoordFiles(coordFile, cursor, geneFasta, proFasta,sampleMapper=None):
        try:
            if geneFasta is not None:
                geneDic = SeqIO.index(geneFasta, 'fasta')
            if proFasta is not None:
                proDic = SeqIO.index(proFasta, 'fasta')
        except Exception as e:
            print(f"{e}")
            return False

        contigsAcc = os.path.basename(coordFile).split('_coords.txt')[0]
        if sampleMapper is not None:
            contigsAcc = sampleMapper[contigsAcc.lower()]

        with open(coordFile) as hdl:
            ccount = 1
            pror = ProdigalResult()

            for ll in hdl:
                if ll[0] == '#':  # First #-line
                    ccount = 1
                    continue
                else:
                    lineList = ll.split(';')
                    tstr = lineList[0].split()
                    pror.tag = tstr[0]
                    pror.no = ccount
                    ccount += 1
                    pror.startIdx = int(tstr[3])
                    pror.endIdx = int(tstr[4])
                    pror.strand = ('F', 'R')[tstr[6] == '-']
                    pror.partial = lineList[1].split('=')[1]
                    pror.startType = lineList[2].split('=')[1]
                    pror.rbsMotif = lineList[3].split('=')[1]
                    pror.rbsSpacer = lineList[4].split('=')[1]
                    pror.gccontent = float(lineList[5].split('=')[1])
                    pror.conf = float(lineList[6].split('=')[1])
                    pror.score = float(lineList[7].split('=')[1])
                    pror.csscore = float(lineList[8].split('=')[1])
                    pror.ssscore = float(lineList[9].split('=')[1])
                    pror.rscore = float(lineList[10].split('=')[1])
                    pror.uscore = float(lineList[11].split('=')[1])
                    pror.tscore = float(lineList[12].split('=')[1])
                    if proFasta is not None:
                        pror.proseq = "'{}'".format(str(proDic[pror.tag + "_" + str(pror.no)].seq))
                    else:
                        pror.proseq = None
                    if geneFasta is not None:
                        pror.dnaseq = "'{}'".format(str(geneDic[pror.tag + "_" + str(pror.no)].seq))
                    else:
                        pror.dnaseq = None

                    try:
                        cursor.execute("insert into prodigal(Id,contigID,startIdx,endIdx,strand,startType,"
                                       "rbsMotif,rbsspacer,gccontent,conf,score,csScore,ssScore,rsScore,"
                                       "uscore,tscore,dnaSeq,proSeq) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
                                       , (
                                       int(pror.tag + "0" + str(pror.no)), contigsAcc, pror.startIdx, pror.endIdx, pror.strand,
                                       pror.startType, pror.rbsMotif, pror.rbsSpacer,
                                       pror.gccontent, pror.conf, pror.score, pror.csscore, pror.ssscore, pror.rscore,
                                       pror.uscore, pror.tscore, pror.dnaseq, pror.proseq))
                    except Exception as e:
                        print(f"{e}")
                        return False
        return True

    @staticmethod
    def renameProdigalGenes(inDir, outDir):
        print(f'\n-----Renaming ORFs-----')
        fastaFiles=[]
        for ext in ["*.fna","*.faa"]:
            fastaFiles += glob(os.path.join(inDir,ext))

        for prog,ff in enumerate(fastaFiles):
            print(f'\rProgress:{prog+1} of {len(fastaFiles)}',end='')
            recList=[]
            with open(ff) as hdl:
                for rec in SeqIO.parse(hdl,'fasta'):
                    recList.append(SeqRecord(rec.seq,str(rec.id).replace("_","0"),description='',name=''))
            fname = os.path.basename(ff)
            with open(os.path.join(outDir,fname),'w') as hdlOut:
                SeqIO.write(recList,hdlOut,'fasta')