import os
from glob import glob
from random import random, randint
from time import time

from yattag import Doc, indent
from Bio import SeqIO
import pandas as pd
from BioLib.Utility import Runner, DataBaseManager
import json


class PlasmidMapper:
    @staticmethod
    def runNucmer(inDir, plasmidDir, outDir, threads=1, samples_file=None, ref_ext="fna", qry_ext="fna"):

        samples = []
        if samples_file is not None:
            with open(samples_file) as hdl:
                for ll in hdl:
                    smp = ll.strip('\r\n')
                    if len(smp) > 1:
                        samples.append(smp)
        else:
            samples = ["*"]

        qryFiles = []
        for smp in samples:
            qryFiles += glob(os.path.join(inDir, f"{smp}.{qry_ext}"))

        refFiles = glob(os.path.join(plasmidDir, f"*.{ref_ext}"))
        cmdList = []
        for qry in qryFiles:
            qname, _ = os.path.splitext(os.path.basename(qry))
            qname = qname.replace('.', '-')
            for ref in refFiles:
                rname, _ = os.path.splitext(os.path.basename(ref))
                rname = rname.replace('.', '-')
                outF = os.path.join(outDir, f"{qname}__{rname}")
                cmdList.append(f"nucmer -t {round(threads / 2)} -p {outF} {qry} {ref}; "
                               f"show-coords {outF}.delta > {outF}.coords")

        Runner.os_parallel(cmdList, threadNumber=2, quiet=False)

    @staticmethod
    def save2DB(db, coordDir, refFasta, qryFasta):
        print('Reading fasta files...')
        tmpLst = []
        for ff in [refFasta, qryFasta]:
            tmp = dict()
            with open(ff) as hdl:
                for rec in SeqIO.parse(hdl, 'fasta'):
                    tmp[rec.id] = [len(rec.seq), str(rec.description).replace(rec.id, "").strip()]
            tmpLst.append(tmp)

        refDic, qryDic = tmpLst
        coordFiles = glob(os.path.join(coordDir, '*.coords'))
        conn, cur = DataBaseManager.connect(db)
        cmdCnt = 0
        for prog, ff in enumerate(coordFiles):
            print(f'\rprogress: {prog + 1}/{len(coordFiles)}-{cmdCnt}.', end='')
            processFlag = False
            with open(ff) as hdl:
                for ll in hdl:
                    if not (processFlag) and (ll.startswith('=')):
                        processFlag = True
                        continue
                    if processFlag:
                        parts = ll.split('|')
                        refStartIdx = int(parts[0].split()[0])
                        refEndIdx = int(parts[0].split()[1])
                        qryStartIdx = int(parts[1].split()[0])
                        qryEndIdx = int(parts[1].split()[1])
                        refLen = int(parts[2].split()[0])
                        qryLen = int(parts[2].split()[1])
                        idy = float(parts[3].split()[0])

                        refTag = parts[4].split()[0]
                        qryTag = parts[4].split()[1]
                        refcov, qrycov, dscr = None, None, None

                        if refTag in refDic:
                            rlen, _ = refDic[refTag]
                            refcov = round(100.0 * refLen / rlen, 2)
                        else:
                            print(f'could not find {refTag}')

                        if qryTag in qryDic:
                            qlen, dscr = qryDic[qryTag]
                            qrycov = round(100.0 * qryLen / qlen, 2)
                        else:
                            print(f'could not find {qryTag}')

                        if qryLen < 1000:
                            continue

                        try:
                            cur.execute(
                                f"insert into plasmidMapper (contig_Id,accession,rstart,rend,qstart,qend,ralign_len,qalign_len,idty,rcov,qcov,description,qlen) values " \
                                " (?,?,?,?,?,?,?,?,?,?,?,?,?)", (
                                refTag, qryTag, refStartIdx, refEndIdx, qryStartIdx, qryEndIdx, refLen, qryLen, idy,
                                refcov, qrycov, dscr, qlen))
                            cmdCnt += 1
                        except Exception as e:
                            print(e)
                            return

        conn.commit()
        conn.close()

    @staticmethod
    def exportPlasmidCoverage(db, outDir):
        conn, cur = DataBaseManager.connect(db)
        pl_len_th = 1e4

        print('Fetching ARGs and VFs...')
        contigAnnot = {}
        proids = set()
        for col, db in [("ARGs", "card"), ("VFs", "vfdb"), ("VFs", "victor")]:
            cmd = f"SELECT proId,cast(substr(bx.proId,1,instr(bx.proId,0)-1) as integer) contigId," \
                  f" bx.description,pr.startidx,pr.endidx " \
                  f" from blastxdiamond bx, prodigal pr" \
                  f" where pr.id=bx.proid and dbname='{db}' and identity >90 and scov>50"
            for proId, coid, dscr, sidx, eidx in cur.execute(cmd):
                if coid not in contigAnnot:
                    contigAnnot[coid] = []
                if proId not in proids:
                    contigAnnot[coid].append([sidx, eidx, dscr, col])
                proids.add(proId)

        cmd = f"select iso.sample,pl.contig_id,pl.accession,pl.description,pl.qstart,pl.qend,pl.qlen,pl.qstart,pl.qend " \
              f" from plasmidMapper pl,contigs co, isolates as iso where pl.contig_id=co.id and co.isolate_id=iso.id " \
              f" and qlen>{pl_len_th} and rcov>50"
        mapDic = {}
        tic = time()
        prog = 0
        print('Fetching the mapping info...')
        for s, co, ac, ds, qs, qe, plen, rs, re in cur.execute(cmd):
            prog += 1
            if time() - tic > 1:
                print(f"\rprogress {prog:,}", end='')
                tic = time()
            key = f"{s}${ac}"
            if key not in mapDic:
                mapDic[key] = {"sample": s, "accession": ac, "description": ds, "length": plen,
                               "indexes": [], "ARGs": None, "VFs": None}
            mapDic[key]["indexes"].append([min(qs, qe),max(qs, qe)])
            dscrDic = {"ARGs": [], "VFs": []}
            if co in contigAnnot:
                for sidx, eidx, dscr, col in contigAnnot[co]:
                    if sidx >= min(rs, re) and eidx < max(rs, re):
                        dscrDic[col].append(dscr)
                mapDic[key]["ARGs"] = "; ".join(sorted(dscrDic["ARGs"]))
                mapDic[key]["VFs"] = "; ".join(sorted(dscrDic["VFs"]))
        print(f"\rprogress {prog}")
        print("Calculating coverage...")
        prog = 0
        for key in mapDic.keys():
            prog += 1
            plasmidLength = mapDic[key]["length"]
            idxLst = mapDic[key]["indexes"]

            if time() - tic > 1:
                print(f"\rprogress {prog:,}/{len(mapDic):,}", end='')
                tic = time()
            tt = 0
            while tt < len(idxLst):
                s_qs, s_qe = idxLst.pop(0)
                removIdx = []
                tt += 1
                for i, ix in enumerate(idxLst):
                    qs, qe = ix
                    if (s_qe - qs) * (qe - s_qs) >= 0:
                        s_qs = min(qs, s_qs)
                        s_qe = max(qe, s_qe)
                        removIdx.append(i)
                        tt = 0
                for idx in sorted(removIdx, reverse=True):
                    idxLst.pop(idx)
                idxLst.append([s_qs, s_qe])
            GetLen = lambda x: abs(x[0] - x[1])
            mapDic[key]["coverage"] = round(100. * sum(list(map(GetLen, idxLst))) / plasmidLength, 2)
            del mapDic[key]["indexes"]

        print(f"\rprogress {prog}/{len(mapDic)}")
        print('saving results in csv...')
        df = pd.DataFrame(list(mapDic.values()))
        df = df[df["coverage"] > 30]
        df = df[["sample", "accession", "coverage", "length", "description", "ARGs", "VFs"]]
        df.to_csv(os.path.join(outDir, "plasmidsCoverage.csv"))
        print('Finished!')

    @staticmethod
    def exportContigCoverage(db, outDir):
        conn, cur = DataBaseManager.connect(db)
        pl_len_th = 1e4

        print('Fetching ARGs and VFs...')
        contigAnnot = {}
        proids = set()
        for col, db in [("ARGs", "card"), ("VFs", "vfdb"), ("VFs", "victor")]:
            cmd = f"SELECT proId,cast(substr(bx.proId,1,instr(bx.proId,0)-1) as integer) contigId," \
                  f" bx.description,pr.startidx,pr.endidx " \
                  f" from blastxdiamond bx, prodigal pr" \
                  f" where pr.id=bx.proid and dbname='{db}' and identity >90 and scov>50"
            for proId, coid, dscr, sidx, eidx in cur.execute(cmd):
                if coid not in contigAnnot:
                    contigAnnot[coid] = []
                if proId not in proids:
                    contigAnnot[coid].append([min(sidx, eidx),max(sidx, eidx), dscr, col])
                proids.add(proId)

        cmd = f"select iso.sample,pl.contig_id,pl.accession,pl.description,co.length,pl.rstart,pl.rend " \
              f" from plasmidMapper pl,contigs co, isolates as iso where pl.contig_id=co.id and co.isolate_id=iso.id " \
              f" and qlen>{pl_len_th}"
        mapDic = {}
        tic = time()
        prog = 0
        print('Fetching the mapping info...')
        for s, co, ac, ds, colen, rs, re in cur.execute(cmd):
            prog += 1
            if time() - tic > 1:
                print(f"\rprogress {prog:,}", end='')
                tic = time()
            key = f"{s}${co}"
            if key not in mapDic:
                mapDic[key] = {"sample": s, "contig_Id": co, "length": colen,
                               "indexes": [], "ARGs": None, "VFs": None, "coverage": 0}

            mapDic[key]["indexes"].append([rs, re])
            dscrDic = {"ARGs": [], "VFs": []}
            if co in contigAnnot:
                for sidx, eidx, dscr, col in contigAnnot[co]:
                    dscrDic[col].append(dscr)
                mapDic[key]["ARGs"] = "; ".join(sorted(dscrDic["ARGs"]))
                mapDic[key]["VFs"] = "; ".join(sorted(dscrDic["VFs"]))

        print("\nCalculating coverage...")
        prog = 0
        for key in mapDic.keys():
            prog += 1
            contigLength = mapDic[key]["length"]
            idxLst = mapDic[key]["indexes"]
            if time() - tic > 1:
                print(f"\rprogress {prog:,}/{len(mapDic):,}", end='')
                tic = time()
            tt = 0
            while tt < len(idxLst):
                s_qs, s_qe = idxLst.pop(0)
                removIdx = []
                tt += 1
                for i, ix in enumerate(idxLst):
                    qs, qe = ix
                    if (s_qe - qs) * (qe - s_qs) >= 0:
                        s_qs = min(qs, s_qs)
                        s_qe = max(qe, s_qe)
                        removIdx.append(i)
                        tt = 0
                for idx in sorted(removIdx, reverse=True):
                    idxLst.pop(idx)
                idxLst.append([s_qs, s_qe])
            GetLen = lambda x: abs(x[0] - x[1])
            mapDic[key]["coverage"] = round(100. * sum(list(map(GetLen, idxLst))) / contigLength, 2)
            del mapDic[key]["indexes"]

        print(f"\rprogress {prog}/{len(mapDic)}")
        print('saving results in csv...')
        df = pd.DataFrame(list(mapDic.values()))
        # df = df[["sample", "accession", "coverage", "length", "description", "ARGs", "VFs"]]
        df.to_csv(os.path.join(outDir, "contigsCoverage.csv"))
        print('Finished!')


class Visualizer:
    @staticmethod
    def export(refDB, plasmidAcc_csv, outDir, data_json=None):
        if data_json is None:
            conn, cur = DataBaseManager.connect(refDB)
            print('Reading combination of plasmids, sample dataframe')
            plSa_DF = pd.read_csv(plasmidAcc_csv, header=0)
            keyTup = plSa_DF.values.tolist()
            samples, accessions = zip(*keyTup)
            keys = {f"{s}${ac}": None for s, ac in keyTup}
            Quotate = lambda x: f"'{x}'"
            print('Fetching plasmid map info...')
            cmd = f"select iso.sample,iso.id,pl.contig_id,pl.accession,pl.description,pl.qstart,pl.qend,pl.rstart,pl.rend,pl.qlen " \
                  f"from plasmidMapper pl,contigs co, isolates as iso where pl.contig_id=co.id and co.isolate_id=iso.id " \
                  f"and sample in ({','.join(list(map(Quotate, samples)))})"
            mapDic = {}
            annotationDic = {}
            tic = time()
            prog = 0
            for s, io, ci, ac, ds, qs, qe, rs, re, plen in cur.execute(cmd):
                prog += 1
                if time() - tic > 1:
                    print(f"\rprogress {prog}", end='')
                    tic = time()
                key = f"{s}${ac}"
                if key not in keys:
                    continue
                if key not in mapDic:
                    mapDic[key] = {"accession": ac, "description": ds, "length": plen, "contigs": []}
                mapDic[key]["contigs"].append({"contigId": ci, "qstart": qs, "qend": qe,
                                               "rstart": rs, "rend": re, "strand": [-1, 1][qs < qe]})
                annotationDic[ci] = []

            print('\nFetching annotations...')
            dbnames = ["card", "vfdb", "victor"]#, "nr"
            proIds = {}
            for db in dbnames:
                print(f"fetching records from {db}")
                cmd = f"select pr.id,cast(substr(pr.id,1,instr(pr.id,0)-1) as integer) contigId,pr.startIdx,pr.endIdx,pr.strand,bx.dbName,bx.identity,bx.scov,bx.description " \
                      f" from prodigal as pr, blastxDiamond bx where pr.id=bx.proId and dbname='{db}' " \
                      f" and identity> 90 and scov>50 order by contigId,startIdx"

                for prid, ci, sidx, eidx, st, db, idt, cov, dscr in cur.execute(cmd):
                    if prid in proIds:
                        continue
                    proIds[prid] = None
                    if ci in annotationDic:
                        annotationDic[ci].append({"id": prid, "sidx": sidx, "eidx": eidx, "idty": idt, "cov": cov,
                                                  "dscr": dscr, "strand": [-1, 1][st == "F"], "db": db})

            plasmidMapp = {"annotation": annotationDic, "mapper": mapDic}
            print('Writing into json...')
            with open(os.path.join(outDir, "plasmidAnnotations.json"), 'w') as hdl:
                json.dump(plasmidMapp, hdl)

            for key in plasmidMapp["mapper"].keys():
                Visualizer.makeHtml(key, plasmidMapp, outDir)
        else:
            print('open json...')
            with open(data_json) as hdl:
                plasmidMapp = json.load(hdl)
            for key in plasmidMapp["mapper"].keys():
                Visualizer.makeHtml(key, plasmidMapp, outDir)

    @staticmethod
    def makeHtml(key, plasmidMapp, outDir):
        sample, accession = key.split('$')
        mapDic = plasmidMapp["mapper"][key]
        annotationDic = plasmidMapp["annotation"]

        r = lambda: randint(0, 255)
        background_hight = 460

        doc, tag, text = Doc().tagtext()

        with tag('html'):
            with tag('head'):
                with tag("link", rel="stylesheet", href="lib/style.css"):
                    text('')
            with tag('body'):
                with tag('plasmid', plasmidheight="2000", plasmidwidth="2000"):
                    doc.attr(sequencelength=str(mapDic["length"]))
                    with tag('plasmidtrack', width="8", trackstyle='fill:#ccc', radius='500'):
                        with tag('tracklabel', vadjust="-30", labelstyle="font-size:18px;font-weight:800"):
                            doc.attr(text=f"{sample}")
                        with tag('tracklabel', vadjust="-10", labelstyle="font-size:18px;font-weight:800"):
                            doc.attr(text=f"{mapDic['accession']}--{mapDic['description']}")
                        with tag('tracklabel', labelstyle="font-size:16px;font-weight:300", vadjust="10"):
                            doc.attr(text=f'{mapDic["length"]} bp')
                        with tag("trackscale", Class='smajor', interval=round(mapDic["length"] / 10), showlabels='1',
                                 labelclass='mdlabel'):
                            text('')
                        # prevSeqId = 0

                        prevSeqId = 0
                        contigLst = sorted(mapDic["contigs"], key=lambda x: abs(x["qstart"] - x["qend"]), reverse=True)
                        coveredRegions = []
                        for coDic in contigLst:

                            pl_sidx = min(coDic["qstart"], coDic["qend"])
                            pl_eidx = max(coDic["qstart"], coDic["qend"])

                            covered = False
                            for s, e in coveredRegions:
                                if pl_sidx >= s and pl_eidx <=e:
                                    covered = True
                            if covered:
                                continue
                            coveredRegions.append([pl_sidx, pl_eidx])
                            # plot regions around contigs; outwards
                            with tag('trackmarker', klass="boundary2", markerstyle="stroke:#9e9e9e;stroke-dasharray:1"):
                                doc.attr(start=str(pl_sidx))
                                doc.attr(wadjust=f"{background_hight}")
                            with tag('trackmarker', klass="boundary2", markerstyle="stroke:#9e9e9e;stroke-dasharray:1"):
                                doc.attr(start=str(pl_eidx))
                                doc.attr(wadjust=f"{background_hight}")
                            with tag('trackmarker', vadjust="10"):
                                doc.attr(start=pl_sidx)
                                doc.attr(end=pl_eidx)
                                doc.attr(wadjust=f"{background_hight - 10}")
                                doc.attr(markerstyle="fill:#0578fa0a")

                            # plot contigs arrow
                            with tag('trackmarker', wadjust="10", vadjust="-3"):

                                doc.attr(start=str(pl_sidx))
                                doc.attr(end=str(pl_eidx))

                                # same color for the same contigs
                                if prevSeqId != coDic["contigId"]:
                                    contigColor = f"#{r():02x}{r():02x}{r():02x}CC"
                                prevSeqId = coDic["contigId"]

                                doc.attr(markerstyle=f'fill:{contigColor}')
                                # direction of the mapped contigs
                                if coDic["strand"] == "F":
                                    doc.attr(arrowendlength='1')
                                    doc.attr(arrowstartlength='-1')
                                else:
                                    doc.attr(arrowendlength='-1')
                                    doc.attr(arrowstartlength='1')
                                with tag('markerlabel', type="path", vadjust="0", hadjust="1", klass="smlabel black"):
                                    cmntText = f"Id: {coDic['contigId']} - {abs(pl_sidx - pl_eidx):,} bp" % ()
                                    doc.attr(text=cmntText)
                                    text('')

                            # plot annotations bounderies
                            co_sidx = min(coDic["rstart"], coDic["rend"])
                            co_eidx = max(coDic["rstart"], coDic["rend"])
                            vadjust_1 = range(30, background_hight, 20)
                            ll, k, i, pre_idx, idx = len(vadjust_1) - 1, 0, 0, 0, 0
                            contigId = str(coDic["contigId"])
                            if contigId not in annotationDic:
                                continue
                            for ann in annotationDic[contigId]:
                                # orf is outside of the mapped region
                                if ann["sidx"] < co_sidx or ann["eidx"] > co_eidx:
                                    continue
                                k += 1
                                # i+=1
                                idx = randint(0, ll)
                                while pre_idx == idx:
                                    idx = randint(0, ll)
                                pre_idx = idx
                                vadj = vadjust_1[idx]
                                if ann['db'] == "nr":
                                    continue
                                orf_sidx = pl_sidx + (ann["sidx"] - co_sidx)
                                orf_eidx = pl_eidx + (ann["eidx"] - co_eidx)
                                with tag('trackmarker', klass="boundary",
                                         markerstyle="stroke:#9e9e9e;stroke-dasharray:1"):
                                    doc.attr(start=orf_sidx)
                                    doc.attr(wadjust="{0}".format(vadj - 3))
                                with tag('trackmarker', klass="boundary",
                                         markerstyle="stroke:#9e9e9e;stroke-dasharray:1"):
                                    doc.attr(start=orf_eidx)
                                    doc.attr(wadjust="{0}".format(vadj - 3))
                                with tag('trackmarker', vadjust="15"):
                                    doc.attr(start=orf_sidx)
                                    doc.attr(end=orf_eidx)
                                    doc.attr(wadjust="{0}".format(vadj - 10))
                                    doc.attr(markerstyle=f"fill:{('#fff', '#fff')[k % 2]}")

                                with tag('trackmarker', wadjust="10"):
                                    doc.attr(start=orf_sidx)
                                    doc.attr(end=orf_eidx)
                                    doc.attr(vadjust=vadj)
                                    doc.attr(klass=ann['db'])
                                    # set the strand of the genes
                                    if ann["strand"] * coDic["strand"] == 1:
                                        doc.attr(arrowendlength=1)
                                        doc.attr(arrowendwidth=1)
                                    else:
                                        doc.attr(arrowstartlength=1)
                                        doc.attr(arrowstartwidth=1)

                                    with tag('markerlabel', type="path", vadjust="2", hadjust="0", valign="outer"):
                                        if ann['db'] != "nr":
                                            doc.attr(text=ann["dscr"], klass="mdlabel black")
                                        text('')

                with tag('script', src="lib/angularplasmid.complete.min.js"):
                    text('')

        outFile = os.path.join(outDir, f"{sample}_{accession}.html")
        with open(outFile, 'w') as hdl:
            hdl.write(indent(doc.getvalue()))
        print(outFile)
