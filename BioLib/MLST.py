
import json
from glob import glob

from datetime import datetime
import xml.dom.minidom as xml
import urllib.request as rq
import os
from urllib.parse import urlparse
from .Utility import Runner, DataBaseManager
import pandas as pd


class MLST:
    @staticmethod
    def getMlst(species, outDir, repository_url='http://pubmlst.org/data/dbases.xml', force_scheme_name=False):

        os.chdir(outDir)

        docFile = rq.urlopen(repository_url)
        doc = xml.parse(docFile)
        root = doc.childNodes[0]
        found_species = []
        for species_node in root.getElementsByTagName('species'):
            info = MLST.__getSpeciesInfo(species_node, species, force_scheme_name)
            if info != None:
                found_species.append(info)
        if len(found_species) == 0:
            print("No species matched your query.")
            exit(1)
        if len(found_species) > 1:
            print("The following {} species match your query, please be more specific:".format(len(found_species)))
            for info in found_species:
                print(info.name)
            exit(2)

        assert len(found_species) == 1
        species_info = found_species[0]
        species_name_underscores = species_info.name.replace(' ', '_')
        species_name_underscores = species_name_underscores.replace('/', '_')
        try:
            os.mkdir(species_name_underscores)
        except Exception as e:
            print(e)
        os.chdir(species_name_underscores)
        # Remove any Bowtie/Samtools index files that already exist.
        for filename in glob(species_name_underscores + '*.bt2'):
            os.remove(filename)
        for filename in glob(species_name_underscores + '*.fai'):
            os.remove(filename)

        # output information for the single matching species
        species_all_fasta_filename = species_name_underscores + '.fasta'
        species_all_fasta_file = open(species_all_fasta_filename, 'wb')
        log_filename = "mlst_data_download_{}_{}.log".format(species_name_underscores, species_info.retrieved)
        log_file = open(log_filename, "w")
        profile_path = urlparse(species_info.profiles_url).path
        profile_filename = profile_path.split('/')[-1]
        log_file.write("definitions: {}\n".format(profile_filename))
        log_file.write("{} profiles\n".format(species_info.profiles_count))
        log_file.write("sourced from: {}\n\n".format(species_info.profiles_url))
        profile_doc = rq.urlopen(species_info.profiles_url)
        profile_file = open(profile_filename, 'wb')
        profile_file.write(profile_doc.read())
        profile_file.close()
        profile_doc.close()
        for locus in species_info.loci:
            locus_path = urlparse(locus.url).path
            locus_filename = locus_path.split('/')[-2] + ".tfa"
            log_file.write("locus {}\n".format(locus.name))
            log_file.write(locus_filename + '\n')
            log_file.write("Sourced from {}\n\n".format(locus.url))
            locus_doc = rq.urlopen(locus.url)
            locus_file = open(locus_filename, 'wb')
            locus_fasta_content = locus_doc.read()
            locus_file.write(locus_fasta_content)
            species_all_fasta_file.write(locus_fasta_content)
            locus_file.close()
            locus_doc.close()
            os.system(f"makeblastdb -dbtype nucl -logfile blast_mkdb.log -in {locus_filename}")
        log_file.write("all loci: {}\n".format(species_all_fasta_filename))
        log_file.close()
        species_all_fasta_file.close()

    # test if a node is an Element and that it has a specific tag name
    @staticmethod
    def __testElementTag(node, name):
        return node.nodeType == node.ELEMENT_NODE and node.localName == name

    # Get the text from an element node with a text node child
    def __getText(element):
        result = ''
        for node in element.childNodes:
            if node.nodeType == node.TEXT_NODE:
                result += node.data
        return MLST.__normaliseText(result)

    # remove unwanted whitespace including linebreaks etc.
    @staticmethod
    def __normaliseText(str):
        return ' '.join(str.split())

    # A collection of interesting information about a taxa
    @staticmethod
    class __SpeciesInfo(object):
        def __init__(self):
            self.name = None  # String name of species
            self.database_url = None  # URL as string
            self.retrieved = None  # date as string
            self.profiles_url = None  # URL as string
            self.profiles_count = None  # positive integer
            self.loci = []  # list of loci

    @staticmethod
    class __LocusInfo(object):
        def __init__(self):
            self.url = None
            self.name = None

    # retrieve the interesting information for a given sample element
    @staticmethod
    def __getSpeciesInfo(species_node, species, exact):
        this_name = MLST.__getText(species_node)
        store = False
        if exact:
            if this_name == species:
                store = True
        else:
            if this_name.startswith(species):
                store = True
        if store:
            info = MLST.__SpeciesInfo()
            info.name = this_name
            for mlst_node in species_node.getElementsByTagName('mlst'):
                for database_node in mlst_node.getElementsByTagName('database'):
                    for database_child_node in database_node.childNodes:
                        if MLST.__testElementTag(database_child_node, 'url'):
                            info.database_url = MLST.__getText(database_child_node)
                        elif MLST.__testElementTag(database_child_node, 'retrieved'):
                            info.retrieved = MLST.__getText(database_child_node)
                        elif MLST.__testElementTag(database_child_node, 'profiles'):
                            for profile_count in database_child_node.getElementsByTagName('count'):
                                info.profiles_count = MLST.__getText(profile_count)
                            for profile_url in database_child_node.getElementsByTagName('url'):
                                info.profiles_url = MLST.__getText(profile_url)
                        elif MLST.__testElementTag(database_child_node, 'loci'):
                            for locus_node in database_child_node.getElementsByTagName('locus'):
                                locus_info = MLST.__LocusInfo()
                                locus_info.name = MLST.__getText(locus_node)
                                for locus_url in locus_node.getElementsByTagName('url'):
                                    locus_info.url = MLST.__getText(locus_url)
                                info.loci.append(locus_info)
            return info
        else:
            return None


    @staticmethod
    def __readSTProfiles(database):
        sts = {}  # key = concatenated string of alleles, value = st
        max_st = 0  # changeable variable holding the highest current ST, incremented when novel combinations are encountered
        header = []
        with open(database, "r") as f:
            for line in f:
                fields = line.rstrip().split("\t")  # remove clonal_complex at the last column
                if fields[-1].startswith('clonal_complex') or fields[-1].startswith('ST') or fields[-1].startswith(
                        'species'):
                    fields.pop()

                if len(header) == 0:
                    header = fields
                    header.pop(0)  # remove st label

                else:
                    sts[",".join(fields[1:])] = fields[0]
                    # finding the maximum of the ST
                    if int(fields[0]) > max_st:
                        max_st = int(fields[0])

        return sts, max_st, header

    @staticmethod
    def runBlastn(samplesFile, contigDir, outDir, dbJSON=None, threads=16, quiet=True):

        samples = []
        with open(samplesFile) as hdl:
            for ll in hdl:
                ll = ll.strip('\r\n')
                samples.append(ll)

        if dbJSON is None:
            # databses = [{"name": "ecoli1", "st_profiles_file": "ecoli.txt", "del": "_","db_dir":""}]
            raise ValueError("No reference database is provided!")

        databases = []
        with open(dbJSON) as hdl:
            species_databses = json.load(hdl)

        for db in species_databses:
            dbName = db["name"]

            dbFile = glob(os.path.join(db["root_dir"], '*.tfa'))
            try:
                tmpdir = os.path.join(outDir, "tmp")
                os.mkdir(tmpdir)
            except Exception as e:
                print(e)
            locus_seqs = {}  # key = id (file name before extension), value = path to sequences

            if len(dbFile) == 0:
                raise ValueError("No reference sequences found!")
            else:
                cmdLst = []
                log_file = os.path.join(tmpdir, f'blast_{dbName}.log')
                for ff in dbFile:
                    (path, fileName) = os.path.split(ff)

                    if not os.path.exists(ff + ".nin"):
                        cmdLst.append(f"makeblastdb -dbtype nucl -logfile {log_file} -in {ff} -out {ff}")
                    (fileName, ext) = os.path.splitext(fileName)
                    locus_seqs[fileName] = ff

                Runner.os_parallel(cmdLst, threads, quiet=quiet)

            for smpl in samples:
                faFile = glob(os.path.join(contigDir, f'{smpl}*'))[0]
                (dir, fileName) = os.path.split(faFile)
                (name, ext) = os.path.splitext(fileName)

                cmdLst = []
                for locus, locus_seq in locus_seqs.items():
                    tmp = os.path.join(tmpdir, "{0}__{1}__{2}.tmp".format(name, dbName, locus))
                    cmd = f"blastn -query {faFile} -db {locus_seq.rstrip()} -max_target_seqs 1 -num_threads {threads} " \
                          f"-outfmt '6 qseqid sacc pident length slen qlen' > {tmp} "
                    cmdLst.append(cmd)
                Runner.os_parallel(cmdLst, round(threads / 2), quiet=quiet)

    @staticmethod
    def summerize(tmpdir, outDir, dbJSON=None):

        if dbJSON is None:
            # species_databses = [{"name": "ecoli1", "st_types": "ecoli.txt", "del": "_","db_dir":""}]
            raise ValueError("No reference database is provided!")

        species_databses = []
        print("loading the databases...")
        with open(dbJSON) as hdl:
            species_databses = json.load(hdl)
        print(f'Found {len(species_databses)} database(s)')

        stDBs = {}
        outDic = {}
        for db in species_databses:

            dbName = db["name"]
            delimeter = db["sep"]
            st_profile = os.path.join(db["root_dir"], db["profiles"])

            sts, max_st, header = MLST.__readSTProfiles(st_profile)
            stDBs[dbName] = [sts, max_st]
            headerIdx = {g: i for i, g in enumerate(header)}

            flist = sorted(glob(os.path.join(tmpdir, f'*{dbName}*.tmp')))
            print(f'\nprocessing {dbName}')

            for prog, ff in enumerate(flist):

                print(f'\rprogress: {prog + 1} of {len(flist)}', end="")

                name, ext = os.path.splitext(ff)
                sampleName, dbName, locus = os.path.basename(name).split('__')
                if (sampleName, dbName) not in outDic:
                    outDic[(sampleName, dbName)] = {"sample": sampleName, 'species': dbName, 'perfect_st': None,
                                                    "best_st": None,
                                                    "genes": ','.join(header),
                                                    "pst_pattern": ["-" for g in header],
                                                    "bst_pattern": ["-" for g in header],
                                                    "bst_comment": ["-" for g in header]
                                                    }
                if os.stat(ff)[6] != 0:
                    # file is not empty, ie match found
                    match = ""
                    # finding the best hit
                    hits = []
                    with open(ff) as hdl:
                        for ll in hdl:
                            fields = ll.rstrip().split("\t")
                            if len(fields) < 6:
                                continue
                            fields.insert(0, float(fields[3]) / float(fields[4]) * float(fields[2]))
                            hits.append(fields)

                    hits.sort(key=lambda tup: tup[0], reverse=True)
                    fields = hits[0]  # selecting the best hit in all contigs
                    fields.pop(0)  # removing the scale we added above

                    (contig, allele, pcid, length, allele_length, contig_length) = (
                        fields[0], fields[1].split(delimeter)[1], float(fields[2]), int(fields[3]), int(fields[4]),
                        int(fields[5]))
                    if pcid == 100.00 and allele_length == length:
                        outDic[(sampleName, dbName)]["pst_pattern"][headerIdx[locus]] = allele
                    elif pcid > 90 and float(length) / float(allele_length) > 0.9:
                        match = f":[{round(pcid, 2)}%;{round(100.0 * float(length) / float(allele_length), 2)}%]"
                        outDic[(sampleName, dbName)]["bst_pattern"][headerIdx[locus]] = allele
                        outDic[(sampleName, dbName)]["bst_comment"][headerIdx[locus]] = allele + match

        print("\nfinding perfect and best STs...")
        for key, cntDic in outDic.items():
            _, dbName = key
            sts, max_st = stDBs[dbName]

            pst_patt = ','.join(cntDic["pst_pattern"])
            bst_patt = ','.join(cntDic["bst_pattern"])
            cntDic["pst_pattern"] = pst_patt
            cntDic["bst_pattern"] = bst_patt

            if pst_patt in sts:
                pst = sts[pst_patt]
            else:
                if "-" not in pst_patt:
                    max_st += 1  # new combination
                    sts[pst_patt] = str(max_st) + '_M'
                    pst = str(max_st) + '_M'
                else:
                    pst = "0"
            if bst_patt in sts:
                bst = sts[bst_patt]
            else:
                if "-" not in bst_patt:
                    max_st += 1  # new combination
                    sts[bst_patt] = str(max_st) + '_M'
                    bst = str(max_st) + '_M'
                else:
                    bst = "0"

            cntDic["perfect_st"] = pst
            cntDic["best_st"] = bst
            cntDic["bst_comment"] = ','.join(cntDic["bst_comment"])

        outF = os.path.join(outDir, f"MLST_{datetime.now().strftime('%Y%b%d-%H-%M-%S')}.csv")
        df = pd.DataFrame(outDic.values())
        df.to_csv(outF)

    @staticmethod
    def save2DB(db,mlstResults):
        conn,cur = DataBaseManager.connect(db)
        df = pd.read_csv(mlstResults,header=0,index_col=0)
        Strip = lambda x:(None,x)[len(x.replace('-', '').replace(',', ''))>0]
        for _, row in df.iterrows():
            try:
                conn.execute(f"update isolates set perfect_ST=?,best_ST=?,genes=?,"
                             f"pst_pattern=?,bst_pattern=?,bst_comment=?,species=? where fq_1 like '%%{row['sample']}%%'",
                             (row["perfect_st"],row["best_st"],row["genes"],Strip(row["pst_pattern"])
                              ,Strip(row["bst_pattern"]),Strip(row["bst_comment"]),row["species"]))


            except Exception as e:
                print(e)
                return
        conn.commit()
