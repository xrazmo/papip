from time import time

import os
from glob import glob
from BioLib.Utility import Runner, DataBaseManager
import pandas as pd

class Hmmer:
    @staticmethod
    def run(seqDir, outDir, modelDir,samples_file=None, instanceNum=5,
                 threadNum=10, mdExtenstion='hmm', seqExtension='faa', stringOption='-E 0.1'):

        samples = []
        if samples_file is not None:
            with open(samples_file) as hdl:
                for ll in hdl:
                    smp = ll.strip('\r\n')
                    if len(smp) > 1:
                        samples.append(smp)
        else:
            samples = ["*"]

        seqFileList = []
        for smp in samples:
            seqFileList += glob(os.path.join(seqDir, f"{smp}*.{seqExtension}"))

        mdFileList = glob(os.path.join(modelDir, '*.{}'.format(mdExtenstion)))

        print("Running models on query sequences...")

        cmdList = []
        for sff in seqFileList:
            seqFileName = os.path.basename(sff).split('.')[0]
            for md in mdFileList:
                modelName = os.path.basename(md).split('.')[0]
                outFile = os.path.join(outDir, '{0}_{1}.out'.format(seqFileName, modelName))
                domtbloutFile = os.path.join(outDir, '{0}_{1}.domtblout'.format(seqFileName, modelName))
                command = f'hmmsearch --cpu {threadNum}  {stringOption} --domtblout={domtbloutFile}  {md} {sff} > {outFile}'
                cmdList.append(command)

        Runner.os_parallel(cmdList,threadNumber=instanceNum,quiet=False)


    @staticmethod
    def saveHmmerResult(db, resultDir):

        domtbloutList = glob(os.path.join(resultDir, '*.domtblout'))
        conn,cur = DataBaseManager.connect(db)
        for ff in domtbloutList:
            Hmmer.__saveFile(cur, ff)
        conn.commit()
        conn.close()
    @staticmethod
    def __saveFile(cursor, domtbloutFile):
        with open(domtbloutFile) as hdl:
            prog = 0
            print(domtbloutFile)
            tic = time()
            skip = 0
            for line in hdl:
                prog += 1
                if line.startswith('#'):
                    continue
                parts = line.split()
                if time()-tic > 0.1:
                    print(f'\rProcessed Lines:{prog}, skipped {skip}',end='')
                    tic = time()
                qlen =  int(parts[5])
                foundLen = abs(int(parts[15])-int(parts[16]))
                coverage = round(100.0*foundLen/float(qlen))
                if coverage<20:
                    skip+=1
                    continue
                cursor.execute(
                    "insert into proteinDomain(proId,tlen,qName,accession,qlen,fEval,fscore,fbias,domainNum,totalDomain"
                    " ,cEvalue,iEvalue,score,bias,hmmFrom,hmmTo,aliFrom,aliTo,envFrom,envTo,acc,coverage) values "
                    "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                    (parts[0], parts[2], parts[3], parts[4], int(parts[5]), parts[6], float(parts[7]),
                     float(parts[8]), int(parts[9]), int(parts[10]), parts[11], parts[12], float(parts[13]),
                     float(parts[14]), int(parts[15]), int(parts[16]), int(parts[17]), int(parts[18]), int(parts[19]),
                     int(parts[20]), float(parts[21]),coverage))

            print(f'\rProcessed Lines:{prog}, skipped {skip}')


class PfamManager:
    @staticmethod
    def getPfamParam(pfamdb,outDir):
        headers = ["NAME","ACC","DESC","LENG","ALPH","RF","MM","CONS","CS","MAP","DATE","NSEQ","EFFN","CKSUM",
                   "GA","TC","NC","BM","SM","STATS LOCAL MSV","STATS LOCAL VITERBI","STATS LOCAL FORWARD"]
        params= []
        tmpDic= {}
        with open(pfamdb) as hdl:
            for ll in hdl:
                ll = ll.strip('\r\n')
                if ll.startswith('HMM '):
                    params.append(tmpDic)
                    tmpDic = {}
                    skip=True
                    continue
                if ll.startswith('HMMER3/f'):
                    skip=False
                    continue
                if not skip:
                    pp = ll.split()
                    hLst = list(filter(lambda x: ll.find(x)>-1,headers))
                    hh, value = hLst[0], ' '.join(pp[1:]).strip(' ')
                    if hLst[0].startswith('STATS'):
                        hh,value = hLst[0],' '.join(pp[3:]).strip(' ')
                    tmpDic[hh] =value

        df = pd.DataFrame(params)
        df.to_csv(os.path.join(outDir,"pfam_metaData.csv"))
