import os
from time import time

from .Utility import Runner

class MMSeqs2:
    @staticmethod
    def createDB(inFasta,outDir):
        print('Convert input fasta to MMSeqs database...')
        dbname = os.path.basename(inFasta).split('.')[0]
        dbDir = os.path.join(outDir, f"db_{dbname}")
        MMSeqs2.__makeDir(dbDir)
        outF_db = os.path.join(dbDir, f"db_{dbname}")
        cmd = f"mmseqs createdb {inFasta} {outF_db}"
        Runner.os_parallel([cmd],1,quiet=False)
    @staticmethod
    def Cluster(dbPath, outDir, workflow='linclust', sims=None, threads=16):
        if sims is None:
            sims = [0.9]
        cmdList = []
        for sim in sims:
            newOutDir = os.path.join(outDir, f"idty-{round(100 * sim)}")
            if not MMSeqs2.__makeDir(newOutDir) :
                return -1
            representativeDir = os.path.join(newOutDir, "representativeDB")
            cluDir = os.path.join(newOutDir, "clus")

            if not MMSeqs2.__makeDir(representativeDir) :
                return -1
            if not MMSeqs2.__makeDir(cluDir) :
                return -1

            postfix = os.path.basename(dbPath)
            clstFiles = os.path.join(cluDir, f'clst_{postfix}')
            repFiles = os.path.join(representativeDir, f'rep_{postfix}')
            cmdList.append( f"mmseqs {workflow} {dbPath} {clstFiles} tmp --split-memory-limit 16G --min-seq-id {sim} --threads {threads};"
                            f"mmseqs createsubdb {clstFiles} {dbPath} {repFiles};"
                            f"mmseqs createtsv {dbPath} {dbPath}  {clstFiles}  {os.path.join(outDir, f'clusters_labels_{round(sim*100)}-{postfix}.tsv')}")
        Runner.os_parallel(cmdList,1,quiet=False)

    @staticmethod
    def __makeDir(path):
        try:
            os.mkdir(path)
            return True
        except Exception as e:
            print(e)
            return False

    @staticmethod
    def parseMMseq(in_tsv, outDir, tblname=None, columnName=None, extraCondi=""):
        if columnName is None:
            raise ValueError('specify the column...')
        if tblname is None:
            raise ValueError('specify the table...')

        print('Reading input file...')

        tic = time()
        command = []

        with open(in_tsv) as hdl:
            prog = 0
            for ll in hdl:
                prog += 1
                toc = time()
                if toc - tic > 2:
                    print(f"\rProg: {prog}", end='', flush=True)
                    tic = time()
                ll = ll.strip('\r\n')
                seed, proId = ll.split()
                command.append(f"update {tblname} set {columnName}={seed} where {extraCondi} id= {proId};")

        print('\nSaving commands...')

        outF = os.path.join(outDir, f'{os.path.basename(in_tsv).split(".")[0]}.sql')
        with open(outF, 'w') as hdl:
            hdl.write('\n'.join(command))
