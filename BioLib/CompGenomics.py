from time import time

import json
import collections
import os
import pandas as pd

from BioLib.Utility import DataBaseManager


class ORFBrowser:
    @staticmethod
    def pairwiseEXORF(db, db_out):
        conn, cur = DataBaseManager.connect(db)
        print('Fetching all the ORFs...')
        isolateDic = {}
        clustSim = 80
        cmd = f"select pl.id,pl.clust_{clustSim},pl.isolate_id from prodigal pl"
        for _, clust, isoId in cur.execute(cmd):
            if isoId not in isolateDic:
                isolateDic[isoId] = []
            isolateDic[isoId].append(clust)

        print('Converting list to set')
        isolateDic = dict(map(lambda x: (x[0], set(x[1])), isolateDic.items()))

        orf_diff = {}
        # orf_diff_T={}
        conn_o, cur_o = DataBaseManager.connect(db_out)
        print('Identifying exclusive sets...')
        isolateIds = list(isolateDic.keys())
        for i in range(len(isolateIds)):
            print(f'\r progress {i + 1}/{len(isolateIds)}', end='')
            iso_i = isolateIds[i]
            for j in range(i + 1, len(isolateIds)):
                iso_j = isolateIds[j]
                # orf_diff[f"{iso_i}${iso_j}"] = list(isolateDic[iso_i].difference(isolateDic[iso_j]))
                # orf_diff[f"{iso_j}${iso_i}"] = list(isolateDic[iso_j].difference(isolateDic[iso_i]))
                ORFBrowser.__add2db(iso_i, iso_j, list(isolateDic[iso_i].difference(isolateDic[iso_j])), cur_o)
                ORFBrowser.__add2db(iso_j, iso_i, list(isolateDic[iso_j].difference(isolateDic[iso_i])), cur_o)
        conn_o.commit()
        print('\nFinished!')

    @staticmethod
    def pairwiseJaccard(db, outDir):
        conn, cur = DataBaseManager.connect(db)
        print('Fetching isolate info')
        cmd = "select id,sample,perfect_st,loc from isolates where perfect_ST is not null order by perfect_st"
        id2sample = {i: [s, st, loc] for i, s, st, loc in cur.execute(cmd)}

        print('Fetching all the ORFs...')
        isolateDic = {}
        clustSim = 80
        cmd = f"select pl.id,pl.clust_{clustSim},pl.isolate_id from prodigal pl " \
              f" where pl.id in (select proid from blastxdiamond " \
              f" where dbname in ('vfdb','victor') and identity>=80 and scov>=50)"
        for _, clust, isoId in cur.execute(cmd):
            if isoId not in isolateDic:
                isolateDic[isoId] = []
            isolateDic[isoId].append(clust)
        indexes, sts, locations = map(list, zip(*id2sample.values()))
        df = pd.DataFrame(0.0, index=indexes, columns=["st", "loc"] + indexes)
        df['st'] = sts
        df['loc'] = locations

        print('Converting list to set')
        isolateDic = dict(map(lambda x: (x[0], set(x[1])), isolateDic.items()))

        print('Identifying exclusive sets...')
        isolateIds = list(isolateDic.keys())
        for i in range(len(isolateIds)):
            print(f'\r progress {i + 1}/{len(isolateIds)}', end='')
            iso_i = isolateIds[i]
            for j in range(i + 1, len(isolateIds)):
                iso_j = isolateIds[j]
                jcc = 1. - round(float(len(isolateDic[iso_i].intersection(isolateDic[iso_j])))
                                 / len(isolateDic[iso_i].union(isolateDic[iso_j])), 4)
                s1 = id2sample[int(iso_i)][0]
                s2 = id2sample[int(iso_j)][0]
                df.at[s1, s2] = jcc
                df.at[s2, s1] = jcc

        print('\nSaving dataframe...')
        df.to_csv(os.path.join(outDir, f"jcc_isolates_vf_cl{clustSim}.csv"))
        print('\nFinished!')

    @staticmethod
    def __add2db(iso1, iso2, orfs, cur):
        for o in orfs:
            cur.execute("insert into EXORFs (isolate_1,isolate_2,proId) values (?,?,?)", (iso1, iso2, o))

    @staticmethod
    def exportCountMat(db, outDir):
        conn, cur = DataBaseManager.connect(db)
        print('Fetching isolate info')
        cmd = "select id,sample,perfect_st from isolates where perfect_ST is not null order by perfect_st"
        id2sample = {i: [s, st] for i, s, st in cur.execute(cmd)}
        print('Creating dataframe...')
        indexes, sts = map(list, zip(*id2sample.values()))
        df = pd.DataFrame(0, index=["st"] + indexes, columns=indexes)
        df.loc['st'] = sts
        print('Grouping exclusive orfs...')
        cmd = "select isolate_1,isolate_2,count(proId) from  EXORFs group by isolate_1,isolate_2;"
        prog = 0
        tic = time()
        for iso_1, iso_2, cnt in cur.execute(cmd):
            prog += 1
            if time() - tic > 1:
                print(f'\rprogress {prog}.', end='')
                tic = time()
            s1 = id2sample[int(iso_1)][0]
            s2 = id2sample[int(iso_2)][0]
            df.at[s1, s2] = cnt
        print(f'\rprogress {prog}.')
        print('Saving dataframe...')
        df.to_csv(os.path.join(outDir, "orf_dist_count.csv"))
        print('Finished!')

    @staticmethod
    def exportExORF_functions(db, outDir, selected_STs=None):
        if selected_STs is None:
            raise ValueError("list of STs are required!")

        conn, cur = DataBaseManager.connect(db)

        print('Fetching isolate info')

        clustSim = 80
        freqTh = 20

        print('fetching st counts...')
        cmd = f"select perfect_ST,count(Id) from isolates" \
              f" where perfect_ST in ({','.join(map(str, selected_STs))}) group by perfect_ST;"
        stCounts = {st: cnt for st, cnt in cur.execute(cmd)}

        print('fetching st & orf counts...')
        cmd = f"select iso.perfect_ST,clust_{clustSim},count(distinct pr.isolate_id) " \
              f" from isolates as iso, prodigal as pr where pr.isolate_id=iso.id and iso.perfect_ST in ({','.join(map(str, selected_STs))})" \
              f" group by iso.perfect_ST,clust_80; "
        stORFsCollection = {}
        for st, clst, cnt in cur.execute(cmd):
            frq = 100.0 * float(cnt) / stCounts[st]
            if frq < freqTh:
                continue
            if st not in stORFsCollection:
                stORFsCollection[st] = set()
            stORFsCollection[st].add(clst)

        print('Finding the differences...')
        diffORFsCollection = {}
        orflist = []
        storfTup = list(stORFsCollection.items())
        for i in range(len(storfTup)):
            st_i, orfs_i = storfTup[i]
            for j in range(i + 1, len(storfTup)):
                st_j, orfs_j = storfTup[j]
                diff_ij = orfs_i.difference(orfs_j)
                diff_ji = orfs_j.difference(orfs_i)
                diffORFsCollection[f"{st_i},{st_j}"] = diff_ij
                diffORFsCollection[f"{st_j},{st_i}"] = diff_ji
                orflist += list(diff_ij) + list(diff_ji)

        print('Fetching annotation data...')
        orflist = list(set(orflist))
        annotations = {}
        dbnames = [("card", 80, 50), ("vfdb", 80, 50), ("victor", 80, 50), ("nr", 30, 50)]
        for db, idty, cov in dbnames:
            print(f"\t-{db}",end='')
            cmd = f"select cast(proid as integer),refid,identity,scov,dbname,description " \
                  f" from blastxdiamond where dbname = '{db}' and identity >= {idty} " \
                  f" and scov>={cov} and proid in ({','.join(map(str, orflist))})"
            cnt = 0
            for rr in cur.execute(cmd):
                if rr[0] not in annotations:
                    annotations[rr[0]] = rr[1:]
                    cnt +=1
            print(f"...{cnt} ORFs.")

        print('integrating annotation data...')
        outDics = []
        for key, orfs in diffORFsCollection.items():
            for orf in orfs:
                st1, st2 = key.split(',')
                refId, idty, cov, dbname, dscr,tag = None, None, None, None, None,'unkown'
                if orf in annotations:
                    refId, idty, cov, dbname, dscr = annotations[orf]
                    tag = ORFBrowser.__getTag(dbname,dscr)
                outDics.append({"st_1": st1, "st_2": st2, "orf_id": orf, "ref_protein": refId, "identity": idty,
                                "coverage": cov, "dbname": dbname,"description":dscr,"tag":tag})

        print('Saving resutls...')
        df = pd.DataFrame(outDics)
        df.to_csv(os.path.join(outDir,f"diff_functions_{'-'.join(map(str, selected_STs))}.csv"))
        print('Finished')

    @staticmethod
    def __getTag(dbname,description):
        tag = None
        if dbname in ['vfdb','victor']:
            tag = 'vf'
        elif dbname =='card':
            tag =  'arg'
        elif description.find('hypothetical')> -1:
            tag = 'hypo'
        elif description.find('transposon') > -1:
            tag = 'tnp'
        return tag