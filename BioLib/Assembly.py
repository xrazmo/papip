import json
import os
from glob import glob
from subprocess import Popen

from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

from BioLib import Utility
from BioLib.Utility import DataBaseManager as dbM
import sqlite3 as lit
from Bio import SeqIO

class Spades:

    @staticmethod
    def __createYAMLFile(inDir, outDir, datasetId, fastq_extension='fq'):

        fqList = sorted(glob(os.path.join(inDir, f"*.{fastq_extension}")))

        aSample = list(filter(lambda x: datasetId in x, fqList))
        unpaired = list(filter(lambda x: "_unpaired_" in x, aSample))
        r1 = list(filter(lambda x: "_val_1.fq" in x, aSample))
        r2 = list(filter(lambda x: "_val_2.fq" in x, aSample))
        outF = os.path.join(outDir, f"{datasetId}.yaml")
        with open(outF, 'w') as hdl:
            json.dump([{"orientation": "fr", "type": "paired-end", "right reads": r1, "left reads": r2},
                       {"type": "single", "single reads": unpaired}], hdl, indent=4)
        return outF

    @staticmethod
    def run(fqDir, outDir, datasetIds_file, threadNum=16):
        print(f"\n{25 * '-'}\n{10 * '*'}SPADES Assembly{10 * '*'}")
        print(f"##Fastq-dir:{fqDir}")
        print(f"##out-dir:{outDir}")
        print(f"##threads:{threadNum}")
        print(f"##datasetIds_file:{datasetIds_file}\n{25 * '-'}")
        print('\tReading dataset IDs...')

        datasetIDs = []
        with open(datasetIds_file) as hdl:
            for ll in hdl:
                ll = ll.strip('\r\n')
                if len(ll) > 1:
                    datasetIDs.append(ll)
        print('\tCreating commands and folders.')
        cmdList = []
        for id in datasetIDs:
            sampleDir = os.path.join(outDir, id)
            try:
                os.mkdir(sampleDir)
            except FileExistsError as e:
                print(f'ERORR:could not create {sampleDir}\n{e}')
                if len(os.listdir(sampleDir)) > 1:  # yaml file is ok.
                    print(f'{20 * "!"}ERORR:Folder is not empty. Skipping sample :({20 * "!"}\n{e}')
                    continue
                print('Usign empty folder..')

            datasetFile = Spades.__createYAMLFile(fqDir, sampleDir, id)
            cmdList.append(f"spades.py -t {threadNum} -k 21,33,55,77 --careful --dataset {datasetFile} -o {sampleDir}")

        Utility.Runner.os_parallel(cmdList, 1, quiet=False)
        print(f"\n{10 * '*'}run-SPADES-FINISHED{10 * '*'}\n{25 * '-'}")

    @staticmethod
    def saveContigs(assDir, db):
        print(f"\n{25 * '-'}\n{10 * '*'}SAVE CONTIGS INTO DATABASE{10 * '*'}")
        print(f"##Assembly_dir:{assDir}")
        print(f"##Sqlite_DB:{db}\n{25 * '-'}")

        print('\tFetching all the samples from sequence DB.')
        conn, cur = dbM.connect(db)
        cmd = "select id, lower(substr(fq_1,1,instr(fq_1,'_1.fastq')-1)) from isolates " \
              " where ticket not in ('failed','doublettes')"
        sampleDic = {rr[1]: rr[0] for rr in cur.execute(cmd)}

        print("\tFinding all the 'contigs.fasta' in the input directory.")
        fnaList = Utility.Runner.read_stdout(f"find {assDir} -name contigs.fasta")

        print('\tCreating a Transaction...')
        conn.execute("BEGIN TRANSACTION;")
        print('\tProcessing contigs files...')
        contigID = 1110
        for i,fa in enumerate(fnaList):
            print(f'\t{i + 1}/{len(fnaList)}: {fa}',end='\t')

            sample = os.path.basename(os.path.dirname(fa)).lower()
            if sample in sampleDic:
                isolatID= sampleDic[sample]
                print(f'{sample}\t{isolatID}')

                with open(fa) as hdl:
                    records = SeqIO.parse(hdl,'fasta')
                    for rec in records:
                        contigID = dbM.nextNonZeroID(contigID)
                        try:
                            conn.execute("insert into contigs (id,isolate_id,seq,description) values (?,?,?,?)",
                                     (contigID,isolatID,str(rec.seq),rec.id))
                        except Exception as e:
                            print(e)
                            print('Rolling back all the changes... :(')
                            conn.execute("ROLLBACK;")
                            conn.close()
                            return -1
            else:
                print(f'{20 * "!"}ERROR:Could not find: {sample}{20 * "!"}')

        print('\tCommitting all the changes...')
        conn.commit()
        conn.close()
        print(f"\n{10 * '*'}saveContigs-FINISHED{10 * '*'}\n{25 * '-'}")

    @staticmethod
    def exportContigs(db,outDir):
        print(f"\n{25 * '-'}\n{10 * '*'}EXPORT CONTIGS FROM DATABASE{10 * '*'}")
        print(f"##OUTDIR:{outDir}")
        print(f"##Sqlite_DB:{db}\n{25 * '-'}")

        conn, cur = dbM.connect(db)
        print('\tFetching all the sequences...')
        cmd = "select cont.id,seq,substr(fq_1,1,instr(fq_1,'_1.fastq')-1) from contigs as cont " \
              " ,isolates as iso where iso.Id=cont.isolate_id order by iso.Id"

        rows = cur.execute(cmd).fetchall()
        rows.append((-1,'',''))
        tmpList = []
        prvSample='#@!'
        print('\tGrouping sequences...')
        for id,sequence,sample in rows:
            if sample!=prvSample and len(tmpList)>0:
                print(f'Exporting {sample}')
                with open(os.path.join(outDir,f'{prvSample}.fna'),'w') as hdl:
                    SeqIO.write(tmpList,hdl,'fasta')
                tmpList=[]
            prvSample=sample
            tmpList.append(SeqRecord(Seq(sequence),id=str(id),description='',name=''))
        print(f"\n{10 * '*'}exportContigs-FINISHED{10 * '*'}\n{25 * '-'}")