# from BioLib import MMSeqs
# from BioLib.Diamond import Diamond
# from BioLib.MLST import MLST
# from BioLib.MMSeqs import MMSeqs2
# from BioLib.PlasmidMapper import PlasmidMapper
# from BioLib.PlasmidMapper import Visualizer as viz
# from BioLib.Prodigal import Prodigal
from BioLib.ProteinDomain import Hmmer
# from BioLib.Utility import Extractor, QualityController as qc, Comperssor as cmp, DataBaseManager as dbm, FastaManager, \
#     DataBaseManager
# from BioLib.Assembly import Spades
import os
import sys, getopt

def main(argv):
    inputfile = ''
    outputfile = ''
    try:
        opts, args = getopt.getopt(argv, "hi:o:", ["ifile=", "ofile="])
    except getopt.GetoptError:
        sys.exit(2)
    for opt, arg in opts:
       if opt in ("-i", "--ifile"):
            inputfile = arg
       elif opt in ("-o", "--ofile"):
            outputfile = arg

    # Spades.run(trimSeqDir, assemblydir, inputfile)
    # cmp.list_tarGZ(inputfile,trimSeqDir,trimSeqGZDir,threads=16)
    # MLST.runBlastn(inputfile,contigsDir, mlstDir,
    #                dbJSON="/crex/proj/snic2021-23-66/private/pa_anotation/MLST/MLST_DB/stDB.json", threads=10,quiet=False)
    # Prodigal.run(contigsDir,prodigalDir,inputfile
    #              ,extension='fna',out_nucl=True,out_prot=True,threads=10)
    # PlasmidMapper.runNucmer(os.path.join(contigsDir,'bysample'),plasmidDBDir,plasmidMapperDir,samples_file=inputfile,threads=16)
    # Diamond.run(os.path.join(prodigalDir, 'genes'), diamondDir,
    #             diamondDB_json=os.path.join(diamondDir, 'diamondDB.json'), samples_file=inputfile,threads=5)
    pfamDir = "/crex/proj/snic2021-23-66/nobackup/databases/pfam"
    Hmmer.run(os.path.join(prodigalDir, 'proteins'), hmmerDir, pfamDir,samples_file=inputfile, instanceNum=2, threadNum=8)

if __name__ == '__main__':
    rootDir = "/crex/proj/snic2021-23-66/private/pa_anotation"
    databaseDir = "/crex/proj/snic2021-23-66/nobackup/databases"
    rawSeqDir = os.path.join(rootDir,"gz")
    trimSeqDir = os.path.join(rootDir,"trim_galore")
    trimSeqGZDir = os.path.join(rootDir,"trim_galore_gz")
    assemblydir = os.path.join(rootDir,"assembly")
    sequenceDB = os.path.join(rootDir,"sequences.db")
    refDB = os.path.join(rootDir,"refDB.db")
    contigsDir = os.path.join(rootDir,"contigs")
    mlstDir = os.path.join(rootDir,"MLST")
    mlstRefDir = os.path.join(mlstDir,"MLST_DB")
    prodigalDir = os.path.join(rootDir,"prodigal")
    hmmerDir = os.path.join(rootDir,"hmmer")

    plasmidDBDir= os.path.join(databaseDir,"ncbi/plasmid")
    plasmidMapperDir =os.path.join(rootDir,"plasmidMapper")
    htmlDir = os.path.join(plasmidMapperDir,"map")

    diamondDir = os.path.join(rootDir,"diamond")
    clusteringDir = os.path.join(rootDir,"clustering")
    # Extractor.find_input(rawSeqDir,extSeqDir,name_re='*.gz',threads=10)
    # qc.trimGalor(rawSeqDir,trimSeqDir,threads=10)

    # Spades.saveContigs(assemblydir,sequenceDB)
    # Spades.exportContigs(sequenceDB,contigsDir)
    # cmp.list_tarGZ("/crex/proj/snic2021-23-66/private/pa_anotation/trim_galore_gz/datasetIDs.txt",trimSeqDir,trimSeqGZDir)
    # MLST.getMlst("Pseudomonas aeruginosa",mlstRefDir)

    # MLST.summerize(os.path.join(mlstDir,'tmp'),mlstDir,dbJSON="/crex/proj/snic2021-23-66/private/pa_anotation/MLST/MLST_DB/stDB.json")
    # dbm.runCommands("/crex/proj/snic2021-23-66/private/pa_anotation/MLST/cmds.sql",sequenceDB,has_commit=True)
    # MLST.save2DB(sequenceDB,"/crex/proj/snic2021-23-66/private/pa_anotation/MLST/MLST_2021Feb23-13-38-44.csv")

    # Prodigal.run(contigsDir, prodigalDir, extension='fna', out_nucl=True, out_prot=True, threads=10)
    # Prodigal.saveResult(refDB,os.path.join(prodigalDir,'tmp'),False,False)
    # Prodigal.renameProdigalGenes(os.path.join(prodigalDir,'tmp'),prodigalDir)

    # PlasmidMapper.runNucmer(os.path.join(contigsDir,'bysample'),plasmidDBDir,plasmidMapperDir,threads=16)
    # PlasmidMapper.save2DB(refDB,os.path.join(plasmidMapperDir,'nucmer'),
    #                       "/crex/proj/snic2021-23-66/private/pa_anotation/contigs/all/all_Contigs.fas",
    #                       "/crex/proj/snic2021-23-66/nobackup/databases/ncbi/plasmid/plasmids_all.fna")

    # FastaManager.concatFiles(os.path.join(contigsDir,'bysample'),contigsDir,"all_Contigs.fas","fna")
    # FastaManager.concatFiles(os.path.join(prodigalDir,'proteins'),prodigalDir,"all_proteins.faa","faa")
    # FastaManager.splitFasta(os.path.join(contigsDir,'all_Contigs.fas'),contigsDir,splitSize=10)

    # Diamond.run(os.path.join(prodigalDir,'genes'),diamondDir,diamondDB_json=os.path.join(diamondDir,'diamondDB.json'),threads=5)
    # Diamond.saveResultsDirectory(os.path.join(diamondDir,'tmp'),["*.b6"])
    # Diamond.findBestHits("/crex/proj/snic2021-23-66/private/pa_anotation/diamond/tmp_blastxDiamond.db")
    # Diamond.import2RefDB(refDB,"/crex/proj/snic2021-23-66/private/pa_anotation/diamond/tmp/tmp_blastxDiamond.db",condi=None)
    # MMSeqs2.createDB(os.path.join(clusteringDir,'all_proteins.faa'),clusteringDir)
    # MMSeqs2.Cluster(os.path.join(clusteringDir,'db_all_proteins/db_all_proteins'),clusteringDir,sims=[1,0.95,0.9,0.8])

    # MMSeqs2.parseMMseq(os.path.join(clusteringDir,'clusters_labels_80-db_all_proteins.tsv'),clusteringDir,tblname="prodigal",columnName="clust_80")
    # DataBaseManager.runCommands_dir(rootDir, refDB, has_commit=True)

    # viz.export(refDB,os.path.join(htmlDir,"sample_accession.csv"),htmlDir)
         # ,data_json=os.path.join(htmlDir,'plasmidAnnotations.json'))
    # pfamDir = "/crex/proj/snic2021-23-66/nobackup/databases/pfam"
    # Hmmer.run(os.path.join(prodigalDir,'proteins'),hmmerDir,pfamDir,samples_file="/crex/proj/snic2021-23-66/private/pa_anotation/hmmer/hmmer_B0.txt",instanceNum=1,threadNum=4)
    Hmmer.saveHmmerResult(refDB,hmmerDir)
    # main(sys.argv[1:])
