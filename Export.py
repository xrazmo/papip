import os
from time import time

from BioLib.Utility import DataBaseManager
import pandas as pd

def exportGeneMatrix(refDB,outDir,metaData_csv=None,clust_sim=100,context='all'):
    conn,cur = DataBaseManager.connect(refDB)
    if metaData_csv is not None:
        metaDF = pd.read_csv(metaData_csv,header=0,index_col=0)
    dbname = "'nr'"
    print(f'Creating matrix of {dbname} and samples')
    idty_th,scov_th=80,80
    condi=""
    if context=='chro':
        condi="and iso.id not in (select co.isolate_id from plasmidMapper as pl, contigs as co where pl.contig_id=co.id and pl.rcov>90 and rlen>10000)"
    if context == 'pl':
        condi = "and iso.id in (select co.isolate_id from plasmidMapper as pl, contigs as co where pl.contig_id=co.id and pl.rcov>90 and rlen>10000)"

    cmd = f"SELECT iso.sample,iso.perfect_st,pr.id as proid,pr.clust_{clust_sim},bx.refid,bx.identity,bx.scov,bx.description " \
          f"FROM prodigal AS pr,isolates AS iso,blastxDiamond AS bx  WHERE pr.isolate_id = iso.id AND " \
          f"pr.id = bx.proid and bx.dbname in ({dbname})  and bx.identity>{idty_th} and bx.scov>{scov_th} {condi};"
    matDic= {}
    print('Fetching orfs.')
    tic = time()
    prog=0
    # matDic["header"]={"sample":"","st":""}
    for sample,st,proid,clustNum,refid,idty,scov,dscr in cur.execute(cmd):
        dscr = dscr.replace(' ', '_').replace('\'', '')
        key = f"{clustNum}_{dscr}"
        prog+=1
        if time()-tic>1:
            print(f'\rprocessed {prog} records.',end='')
            tic=time()
        if sample not in matDic:
            matDic[sample]= {"sample":sample,"st":st}
        if clustNum not in matDic[sample]:
            matDic[sample][key]=0
        matDic[sample][key] += round(idty*scov/1e4,2)
        # matDic["header"][clustNum]=dscr
    print(f'\rprocessed {prog} records.')
    print('converting dictionary to csv')
    df = pd.DataFrame(list(matDic.values()))
    df = df.fillna(0)
    print('Merging with metadata')
    merged_df = pd.merge(df, metaDF, on="sample",how="left")
    merged_df.to_csv(os.path.join(outDir,f'featureMatrix_nr_cl{clust_sim}_{context}.csv'))
    print('Finished!')

if __name__ == '__main__':
    rootDir = "/crex/proj/snic2021-23-66/private/pa_anotation"
    refDB = os.path.join(rootDir, "refDB.db")
    exportDir = os.path.join(rootDir, "exports")
    # for sim in [80,95,100]:
    exportGeneMatrix(refDB,exportDir,metaData_csv=os.path.join(exportDir,'pa_data.csv'),clust_sim=95,context='all')