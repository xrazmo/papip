import os
from subprocess import Popen


def forkAssembly(sbatchTemplateFile,pythonEntryFile,datasetsFile,outDir,jobNr=10):

    os.chdir(os.path.dirname(outDir))

    sbatchTemplate = []
    with open(sbatchTemplateFile) as hdl:
        sbatchTemplate=hdl.readlines()

    with open(datasetsFile) as hdl:
        ids = [ll for ll in hdl]
    chunkSize = round(len(ids)/jobNr)

    for i,batch in enumerate(chunks(ids,chunkSize)):
        outf = os.path.join(outDir,f"asm_B{i}.txt")
        with open(outf,'w') as hdl:
            hdl.write(''.join(batch))

        newsBathcontent=[]
        for ll in sbatchTemplate:
            if ll.startswith("#SBATCH -J"):
                ll=f"#SBATCH -J asm_B{i}\n"
            elif ll.startswith("srun python"):
                ll = f"srun python {pythonEntryFile} -i {outf}"
            newsBathcontent.append(ll)
        outf = os.path.join(outDir, f"paPipRunner_{i}.sh")
        with open(outf, 'w') as hdl:
            hdl.write(''.join(newsBathcontent))
        # submit the job
        print(f"sbatch {outf}")
        Popen(f"sbatch {outf}",shell=True).wait()

def forkGeneral(sbatchTemplateFile,pythonEntryFile,datasetsFile,outDir,jobname,jobNr=10):

    os.chdir(os.path.dirname(outDir))

    sbatchTemplate = []
    with open(sbatchTemplateFile) as hdl:
        sbatchTemplate=hdl.readlines()

    with open(datasetsFile) as hdl:
        ids = [ll for ll in hdl]
    chunkSize = round(len(ids)/jobNr)

    for i,batch in enumerate(chunks(ids,chunkSize)):
        outf = os.path.join(outDir,f"{jobname}_B{i}.txt")
        with open(outf,'w') as hdl:
            hdl.write(''.join(batch))

        newsBathcontent=[]
        for ll in sbatchTemplate:
            if ll.startswith("#SBATCH -J"):
                ll=f"#SBATCH -J {jobname}_B{i}\n"
            elif ll.startswith("srun python"):
                ll = f"srun python {pythonEntryFile} -i {outf}"
            newsBathcontent.append(ll)
        outf = os.path.join(outDir, f"paPipRunner_{i}.sh")
        with open(outf, 'w') as hdl:
            hdl.write(''.join(newsBathcontent))
        # submit the job
        print(f"sbatch {outf}")
        Popen(f"sbatch {outf}",shell=True).wait()

def chunks(lst, n):
   for i in range(0, len(lst), n):
        yield lst[i:i + n]

if __name__ == '__main__':
    # forkAssembly("/domus/h1/xrazmo/private/source_codes/papip/paPipRunner.sh",
    #              "/domus/h1/xrazmo/private/source_codes/papip/workDesk.py",
    #              "/crex/proj/snic2021-23-66/private/pa_anotation/assembly/datasetIDs.txt",
    #              "/crex/proj/snic2021-23-66/private/pa_anotation/assembly",jobNr=10)

    forkGeneral("/domus/h1/xrazmo/private/source_codes/papip/paPipRunner.sh",
                 "/domus/h1/xrazmo/private/source_codes/papip/workDesk.py",
                 "/crex/proj/snic2021-23-66/private/pa_anotation/trim_galore_gz/datasetIDs.txt",
                 "/crex/proj/snic2021-23-66/private/pa_anotation/diamond",jobname='diamond_small',jobNr=10)