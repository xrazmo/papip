import collections

from BioLib.CompGenomics import ORFBrowser
from BioLib.PlasmidMapper import Visualizer as viz, PlasmidMapper
import os
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
import seaborn as sb
import scipy.spatial as sp, scipy.cluster.hierarchy as heiclt
import numpy as np

def plotHeatmap_abundanceTbl(inCSV, outDir):
    df = pd.read_csv(inCSV, index_col=11, low_memory=False)

    hc = ['#bd0026',"#ffffcc" ,'#addd8e', '#004529']
    th = [0, 0.85, 0.90,1]

    cdict = nonLinCdict(th, hc)
    cm = LinearSegmentedColormap('cmCl', cdict)

    print('changing datatype...')
    metaCol = ["st","year","location","sepsisD7","sepD1_swe","Death7","Death30","acqLoc","source","micF","recBac"]
    metaDF = df[metaCol]
    df = df.drop(metaCol, axis=1)
    df = df.apply(pd.to_numeric, errors='ignore')

    print('Plotting figure...')
    plt.figure(figsize=(40, 40))
    sts = metaDF["st"]
    cnt = metaDF["st"].value_counts()
    lut = dict(zip(cnt.index,sb.color_palette()+sb.color_palette("flare",n_colors=100)))
    Z = heiclt.linkage(df, method='complete')
    grid= sb.clustermap(df, row_linkage=Z,cmap=cm,row_colors=sts.map(lut))
    fname = os.path.basename(inCSV).split('.')[0]
    outDF = grid.data2d.loc[grid.data2d.index,grid.data2d.index]
    outDF = pd.merge(outDF,metaDF,left_index=True,right_index=True)
    outDF.to_csv(os.path.join(outDir,f"{fname}_clusterOrder.csv"))
    plt.show()
def getNewick(node, newick, parentdist, leaf_names):
    if node.is_leaf():
        return "%s:%.2f%s" % (leaf_names[node.id], parentdist - node.dist, newick)
    else:
        if len(newick) > 0:
            newick = "):%.2f%s" % (parentdist - node.dist, newick)
        else:
            newick = ");"
        newick = getNewick(node.get_left(), newick, node.dist, leaf_names)
        newick = getNewick(node.get_right(), ",%s" % (newick), node.dist, leaf_names)
        newick = "(%s" % (newick)
        return newick

def nonLinCdict(steps, hexcol_array):
    cdict = {'red': (), 'green': (), 'blue': ()}
    for s, hexcol in zip(steps, hexcol_array):
        rgb =matplotlib.colors.hex2color(hexcol)
        cdict['red'] = cdict['red'] + ((s, rgb[0], rgb[0]),)
        cdict['green'] = cdict['green'] + ((s, rgb[1], rgb[1]),)
        cdict['blue'] = cdict['blue'] + ((s, rgb[2], rgb[2]),)
    return cdict

def grid_peichart(incsv,outdir):
    df = pd.read_csv(incsv, index_col=0, header=0)
    df["tag2"] =df["tag2"].fillna('Other')
    df = df.loc[df["tag"]=="vf"]
    t_df = df[["st_1","st_2","tag2","orf_id"]]
    g_df = t_df.groupby(by=["st_1","st_2","tag2"], dropna=False).agg(["count"])
    # idxDic = collections.OrderedDict([("arg",[0, "ARGs"]),("vf",[1,"VFs"]),("hypo",[2,"Hypothetical Proteins"]),
    #                                   ("tnp",[3,"Transposase"]),("nan",[4,"Other"]),("unknown",[5,"Uknown ORFs"])])
    colorDic = {"Alginate":"#f781bf","Exoenzymes":"#7a0177","Fimbrial biosynthesis":"#006837","Flagellar protein":"#225ea8",
                "Ferripyoverdine receptor":"#ffff33","Pyocyanin":"#e31a1c","Pilus proteins":"#6a51a3",
                "Pyoverdine":"#8c510a","Rhamnolipid biosynthesis":"#35978f",
                "Type IV secretion":"#80cdc1","Type VI secretion":"#4daf4a","LPS biosynthesis":"#fd8d3c",
                "Other":"#bababa"}
    pieData={}
    for kk in g_df.index:
        key = kk[:2]
        cat = kk[-1]
        if key not in pieData:
            pieData[key] = {"d": [], "lbl": [], "cl":[]}
        # if key not in pieData:
        #         _,l =zip(*idxDic.values())
        #     pieData[key]={"d":np.zeros(shape=(len(idxDic),)),"lbl":l}
        # idx,_ = idxDic[cat]
        pieData[key]["d"].append(int(g_df.loc[kk]))
        pieData[key]["lbl"].append(cat)
        pieData[key]["cl"].append(colorDic[cat])

    sts = df['st_1'].unique()
    gridSize = len(sts)
    fig, axs = plt.subplots(gridSize, gridSize,figsize=(15,15))
    labels = ''
    for i in range(gridSize):
        st_i = sts[i]
        for j in range(gridSize):
            st_j = sts[j]
            key = (st_i, st_j)
            if i == 0:
                axs[i, j].set_title(f"ST{st_j}",fontsize=20)
            if j == 0:
                axs[i, j].set_ylabel(f"ST{st_i}",fontsize=20)
            if key not in pieData:
                axs[i,j].pie([1],colors=['#f0f0f0'], shadow=True)
                continue
            data = pieData[key]["d"]
            labels = pieData[key]["lbl"]
            cl = pieData[key]["cl"]
            a = list(zip(data,labels,cl))
            a.sort(key=lambda x:x[1])
            data,labels,cl = zip(*a)
            _, _, autotexts=axs[i,j].pie(data, colors=cl,autopct='%1.1f%%',shadow=True,pctdistance=0.8, startangle=90)
            # for autotext in autotexts:
            #     autotext.set_color('white')
            axs[i,j].axis('equal')

    h,lbls = axs[0, 0].pie(np.zeros(shape=(len(colorDic))), colors=list(colorDic.values()))
    axs[0, 0].legend(h,colorDic.keys(),bbox_to_anchor=(-1.1, 1.9), loc='upper left',ncol=5,fontsize=14)
    # plt.show()
    plt.savefig(os.path.join(outdir,'grid_st_vf.png'))


if __name__ == '__main__':
    rootDir = "C:/Users/Mohammad/archive/KI/pseudomonas/analyses"
    refDB = os.path.join(rootDir,"refDB.db")
    plasmidMapDir = os.path.join(rootDir,"plasmidMap")
    htmlDir = os.path.join(plasmidMapDir,"html")
    compGenomicsDir = os.path.join(rootDir,"comp_genomics")
    isolateORFsAnalysDB = os.path.join(compGenomicsDir,"isolateORFsAnalys.db")

    viz.export(refDB,os.path.join(htmlDir,"sample_accession.csv"),htmlDir
              # ,data_json=None)
           ,data_json=os.path.join(htmlDir,'plasmidAnnotations.json'))
    #
    # PlasmidMapper.exportPlasmidCoverage(refDB,plasmidMapDir)
    # PlasmidMapper.exportContigCoverage(refDB,plasmidMapDir)


    # ORFBrowser.pairwiseEXORF(refDB,isolateORFsAnalysDB)
    # ORFBrowser.exportCountMat(isolateORFsAnalysDB,compGenomicsDir)
    # ORFBrowser.pairwiseJaccard(refDB,compGenomicsDir)
    # ORFBrowser.exportExORF_functions(refDB,compGenomicsDir,selected_STs=[111,244,175,253,235,308])
    # grid_peichart(os.path.join(compGenomicsDir,"diff_functions_111-244-175-253-235-308.csv"),compGenomicsDir)
    # plotHeatmap_abundanceTbl(os.path.join(compGenomicsDir,"jcc_isolates_cl80_merged.csv"),compGenomicsDir)